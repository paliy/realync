Realync.Models.Chat_item = Backbone.Model.extend({
    initialize: function() {
        _.bindAll(this, 'time_send');
        this.set({time_send: this.time_send()})

    },


    time_send: function(){
        console.log('time_send');
        var time = this.get('timeStamp');
        var time_h_m = time.split('T')[1].split(":")[0] + ':' + time.split('T')[1].split(":")[1];
        return time_h_m ;
    },

    url: 'http://localhost:3001/dashboard'
});
