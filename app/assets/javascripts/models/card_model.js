Realync.Models.Card = Backbone.Model.extend({
    initialize: function() {
        card_model = this;
        this.set('type_image', this.select_card())
        this.set('month', this.get('expirationMonth'));
        this.set('year', this.get('expirationYear'));
        this.set('number', this.get('maskedNumber'));
    },

    defaults: {
        cvv: ''
//        last_name: '',
//       expirationMonth: "02",
//       expirationYear: "2018",
//       cardTypeImage: "https://assets.braintreegateway.com/payment_method_logo/visa.png?environment=sandbox&merchant_id=nb9s2mwxqx9sphjb",
//       last4: "0061",
//       expired: false,
//       token: "38t3z6",
//       expirationDate: "02/2018",
//       maskedNumber: "************0061"
    },

    getCards: function(data){
        $.ajax({
            url: localStorage.url + '/user/getCards',
            type: 'GET',
            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                Realync.card_collection.reset();
                $('.isCard').html("");
                for(i in output.cards){
                  var card = new Realync.Models.Card(output.cards[i])
                    Realync.card_collection.add(card)
                }
                Realync.card_collection.each(function(card){
                    var view = new Realync.Views.CardView({model: card})
                    $('.isCard').append(view.render().el);
                }, this)
                $('.card_container').click(function(){
                    var id = $(this).attr('id')
                    var card = _.find(Realync.card_collection.models, function(card){return card.get('last4') == id})

                    $("#change_payment_method").hide();
                    $("#block-payment").show();
                    var payment_form = new Backbone.Form({
                        template: _.template($('#paymentSettingsTemplate').html()),
                        model: card
                    }).render();

                    $('#payment-settings-form').html(payment_form.el);
                    $('[name="last_name"], [name="postal_code"] ').parent().parent().parent().remove();
                    delete $('[name="last_name"], [name="postal_code"] ').parent().parent().parent()

                    $('.back_from_form').click(function(){
                        $("#change_payment_method").show();
                        $("#block-payment").hide();
                        return false;
                    });

                   $('.delete_card').click(function(){
                       card_model.deleteCards(card.get('token'));
                       Backbone.history.loadUrl()
                       return false;
                    });

                    var braintree =  Braintree.create("MIIBCgKCAQEAwhvIrhLu7EICw/7So5ZZhmcfe5jW4W9V55UMI3S3NHyv/UZgP/GCTdrZ56wad2kJY0eVLO5l6VbfZlCM+YqCxd6h7JbyBeTrBDiIrvgiVRl+08oy5nEnTcxYPYo08AMQGtY+rbtF7pDy3kVnmk4kkE6kxyMr7Xou5h7q3zAOtQ0y8T6g690DVopkFRGr3L10Wlx0L52YIkX1R1RHAAgrFk4L6CICp2vCf+2a+1/+iXObit1u2A27Ekrn8GExxnHyJ+s1d67OZd7m4s4ZAUQS/jXn/EketfKdi+FOTnhqAJLq2o5shSj4C61lrVOnk3/Gc4rT5eDVdB125asfaf66qwIDAQAB")
                    braintree.onSubmitEncryptForm('braintree-payment-form', function(){
                        console.log('SUBMIT')
                        var errors = payment_form.commit()
                        delete errors.last_name
                        delete errors.postal_code
                        if(!Object.keys(errors).length > 0){
                            var data = card_model.getFormData('#braintree-payment-form');
                            card_model.updateCards(data, card.get('token') )

                        }

                        console.log(data)
                    });
                    return false;
                })

            },
            error: function(data){
                console.log('Error');
            }

        })

    },

    getFormData: function(form){
        var unindexed_array = $(form).serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    },

   updateCards: function(data, token){
        $.ajax({
            url: localStorage.url + '/user/updateCard/' +  token,
            type: 'POST',
            data: data,
            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output)


            },
            error: function(data){
                console.log('Error');
            }

        })

    },

   deleteCards: function( token){
        $.ajax({
            url: localStorage.url + '/user/deleteCard/' +  token,
            type: 'POST',
            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
                console.log(output)


            },
            error: function(data){
                console.log('Error');
            }

        })

    },

    select_card: function(){
        var type = ''
        var mastercard = new RegExp("mastercard")
        var amex = new RegExp("amex")
        var visa = new RegExp("visa")
        var discover = new RegExp("discover")
        var name = this.get("cardTypeImage");
        if(name){
            if (name.match(mastercard) && name.match(mastercard).length > 0){
                type = '/assets/update_payment/mc.png';
            }else if(name.match(amex) && name.match(amex).length > 0){
                type = '/assets/update_payment/amex.png';
            }else if(name.match(discover) && name.match(discover).length > 0){
                type = '/assets/update_payment/discover.png';
            }else if(name.match(visa) && name.match(visa).length > 0){
                type = '/assets/update_payment/visa.png';
            };
            return type

        }



    },


    schema: {
        postal_code: {
            validators: ['required'],
            title: null,
            editorAttrs: {
                'placeholder': 'Postal Code'
            }
        },
        last_name: {
            validators: ['required'],
            title: null,
            editorAttrs: {
                'placeholder': 'Last Name'
            }
        },

        number: {
            validators: ['required'],
            title: null,
//           type: 'Number',
            editorAttrs: {
                'placeholder': 'Card Number',
                'data-encrypted-name': "number"
            }
        },
        cvv: {
            validators: ['required'],
            title: null,
            type: 'Number',
            editorAttrs: {
                'placeholder': 'Security Code',
                'data-encrypted-name': "cvv"
            }
        },
        month: {
            validators: ['required'],
            title: null,
            type: 'Select',
            editorAttrs: {
                'data-encrypted-name': "month"
            },

            options: [{
                val: null,
                label: 'Expiration Month'
            },
                '01', '02', '03',
                '04', '05', '06', '07', '08',
                '09', '10', '11', '12']
        },
        year: {
            validators: ['required'],
            type: 'Select',
            title: null,
            editorAttrs: {
                'data-encrypted-name': "year"
            },
            options: expirationYear()
        },

        plan:{
            type: "Hidden"
        }



    },

    url: 'http://localhost:3001/dashboard'
});
function expirationYear() {
    var years = [];
    var current_year = new Date().getFullYear();
    years[0] = { val: null, label: 'Expiration Year' };
    years[1] = current_year;
    for (var i = 1; i < 16; i++) {
        years[i+1] = current_year + i;
    };
    // console.log(years);
    return years;
}
