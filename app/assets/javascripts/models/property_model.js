Realync.Models.Property = Backbone.Model.extend({
    initialize: function() {
        this.set("video_checked", this.video_checked())
    },

    schema: {
        street1: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Address'
            },
            title: null
        },
        city: {
        	type: 'Hidden',
            // validators: ['required'],

            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },
        state: {
			type: 'Hidden',

            // validators: ['required'],
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },
        postalCode: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Zip Code'
            },
            title: null
        },
        sqft: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Square Feet'
            },
            title: null
        },
        bathCount: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': '# of Bathrooms'
            },
            title: null
        },
        bedroomCount: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': '# of Bedrooms'
            },
            title: null
        },
        description: {
            validators: ['required'],
            // type: 'TextArea',
            editorAttrs: {
                'placeholder': 'Description'
            },
            title: null
        },
        photoNames: {
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },
        videoName: {
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        }

    },

    video_checked: function(){
        var s = this.get("videoURL")
        if(s){
            var arr = s.split("/")
            return arr[arr.length -1] != "no_video"
        }
    },

    createProperty: function (data, user) {

    	data.realtor = user.attributes.id;
    	data.zipcode = data.postalCode;
    	user.get_params_by_zip(data, 'createProperty');

    },

    getInfo: function (id, type) {
    	$.ajax({
            url: localStorage.url + '/property/show/' + id,

            type: 'POST',


            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
            	console.log(data);
            	data.property.street1 = data.property.address.street1;
            	data.property.postalCode = data.property.address.zip;
                var property = new Realync.Models.Property(data.property);
            	if(type == 'edit') {
            		property.edit_property(property);
            	} else if (type == 'view'){            		
	                console.log("Property View Load....");
	                console.log(data);
	                var view = new Realync.Views.viewProperty({model: property});
	                view.render();
                    $('#property_gallery').carousel({
                        itemsPerTransition: 1,
                        pagination: false,
                        continuous: true,
                        loop: true,
                        insertPrevAction: function () { 
                            return $('<button class="rs-carousel-action rs-carousel-action-prev prev">‹</button>').appendTo(this); 
                        },
                        insertNextAction: function () { 
                            return $('<button class="rs-carousel-action rs-carousel-action-next next">›</button>').appendTo(this); 
                        },
                    });
                    $('.ui-state-disabled').removeClass('ui-state-disabled');
                    $('li img').click(function(){
                        $('#full_size_img img').attr('src', $(this).attr('src'));
                        $('#full_size_img').show();
                    });
                    $('#full_size_img').click(function(){
                        $(this).hide();
                    })
            	}
            },
            error: function(data) {
                console.log("error create prop");
            }
        });
    },

    edit_property: function (property) {


		var form = new Backbone.Form({
            template: _.template($('#editPropertyTemplate').html()),
            model: property
        }).render();

        form.on('submit', function(event) {

            var data = form.getValue();
            var errors = form.commit();

            data.property_id = property.id;
            data.zipcode = data.postalCode;
            if(! errors){
            	var user = new Realync.Models.User;
            	user.get_params_by_zip(data, 'updateProperty');

            }
        });
        // (new Realync.Views.editProperty).render(prop);
		var view = new Realync.Views.editProperty({model: property});
		view.render();

        $('#property').html(form.el);
        if(property.attributes.picURL != null || property.attributes.mobilePicURL != null) {
            $("#property_cover_image").css("background-image", "url(" + property.attributes.picURL + ")"); 
            $('label.cover_image_upload').hide();
            $('label.cover_image_update').css("display", "inline-block");
            $('img.cover_image_remove').show();
        }
        $('#submit_form').val('Save Changes')
        view.upload_cover_photo(property.get('id'));
        view.upload_multiple_images(property.get('id'));
        view.upload_video(property.get('id'));
        var video = property.get('videoURL');
        if(video){
            var arr = video.split('/');
            if(arr[arr.length - 1] != 'no_video'){
                view.add_video_preview(video);
            }
        }

        $('img.cover_image_remove').click(function(){
            var prop = new Realync.Models.Property;
            prop.delete_photo(property.attributes.id, property.attributes.mobilePicURL, true);
        });
        $('a.delete-property-link').click(function(){
            var prop = new Realync.Models.Property;
            prop.delete(property.attributes.id);
        });

        $('#multifiles').prepend(view.show_photos(property.get('photoURLs'), property.get('id')));
//        view.upload_multiple_images(property.get('id'));
    },


    update_property: function(data, id){

        console.log('--------update--------property----------')
        console.log(data)
        console.log(id);
        $.ajax({
            url: localStorage.url + '/property/edit/' + id,

            type: 'POST',
            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
				console.log(output);
				
                var model = Realync.properties_collection.find_property_by_id(id);
                console.log(model);
                model.attributes.address.city = data.city;
                model.attributes.address.state = data.state;
                model.attributes.address.street1 = data.street1;
                model.attributes.address.zip = data.zipcode;
                model.set({
                    bathCount: data.bathCount,
                    bedroomCount: data.bedroomCount,
                    sqft: data.sqft,
                    description: data.description
                });
                console.log(model);
                console.log(data);
                if($('.btn.btn-primary.cover-photo').length) {
                    $('.btn.btn-primary.cover-photo').click();
                } else {
                    window.location = "#!/property/" + id;
                }
                
            },
            error: function(data){
                console.log('Error update property');
            }

        })

    },

    delete: function(id, type){

        console.log('----------property----------remove----------')
        $.ajax({
            url: localStorage.url + '/property/delete',

            type: 'POST',

            data: {
            	propertyID: id
            },

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                
                var model = Realync.properties_collection.remove_property(id);
                console.log("property removed");

                if(type == 'index'){
                    $('#' + id).parent().fadeOut()

                }

                
            },
            error: function(data){
                console.log('Error delete property');
            }

        })

    },

    index: function (type) {

        if(type == 'dashboard'){
     
            var prop = new Realync.Models.Property;
            prop.index('agent');
        }
    	$.ajax({
            url: localStorage.url + '/property/index',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('Ok');
				console.log(output);
				var properties = output.properties;
                Realync.properties_collection.reset();
				for(idx in properties) {
					var prop = new Realync.Models.Property(properties[idx]);
					Realync.properties_collection.add(prop);

				};
                console.log(Realync.properties_collection);


				if(type == 'property_index') {
					var view = new Realync.Views.Property_list;
					view.render();
				} else if (type == 'dashboard') {
                    reset_el_spiner('view-recent-properties');
					var view = new Realync.Views.agentRightbar;
					view.addRecentPropertyList();
				} else if (type == 'new_prop') {
                    //
                };
            },
            error: function(data){
                reset_el_spiner('view-recent-properties');
                console.log('Error update property');
            }

        })
    },

    get_agent_property: function(id) {

        $.ajax({
            url: localStorage.url + '/property/index/' + id,

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(data){
                console.log(data);
                var properties = data.properties;
                Realync.properties_collection.reset();
                for(idx in properties) {
                    var prop = new Realync.Models.Property(properties[idx]);
                    Realync.properties_collection.add(prop);
                };
                var view = new Realync.Views.Property_list;
                view.render();
                $(".for_del").removeClass('large-4').addClass('large-6');
            },
            error: function(data){
                console.log('Error');
            }

        })
    },

    request_tour: function(propertyID) {
        $.ajax({
            url: localStorage.url + '/message/requestTour/' + propertyID,
            type: 'POST',
            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log(data);
            },
            error: function(data) {
                console.log(data);
            }

        });
    },

    set_thumbnail: function (picName, propertyID) {
        set_process_text('set cover photo', 'cover');
        console.log(picName);
        console.log(propertyID);
        $.ajax({
            url: localStorage.url + '/property/setThumbnail/' + propertyID,

            type: 'POST',

            data: {
                picName: picName
            },

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("SUCCESS thumbnail set");
                console.log(data);
                var model = Realync.properties_collection.find_property_by_id(propertyID);
                if(model != undefined) {
                    model.set({ picURL: picName});
                    model.set({ mobilePicURL: picName});
                }

                window.location = "#!/property/" + propertyID;
                $("<style>#prop-cover-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                $('#prop-cover-photo').css('z-index','1');
                Backbone.history.loadUrl();
                check_downloades();
            },
            error: function(data) {
                console.log("error set thumbnail");
                set_process_text("error set thumbnail", 'cover');
                $("<style>#prop-cover-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                $('#prop-cover-photo').css('z-index','1');
                check_downloades();
            }
        });
    },

    delete_photo: function (id, src, cover) {
        $.ajax({
            url: localStorage.url + '/property/deletePhoto/' + id,

            type: 'POST',

            data: {
                picName: src
            },

            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                console.log("photo removed");
                console.log(data);
                var model = Realync.properties_collection.find_property_by_id(id);
                console.log(model);
                if(cover && model != undefined) {
                    alert('asdasdasd');
                    model.set({
                        picURL: "",
                        mobilePicURL: ""
                    });
                    console.log(model);
                }
            },
            error: function(data) {
                console.log("error remove photo");
            }
        });
    },



  url: 'http://localhost:3001/dashboard'
});