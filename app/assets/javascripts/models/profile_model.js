Realync.Models.Profile = Backbone.Model.extend({
    initialize: function() {
//        _.bindAll(this, 'sign_up');
    },
    defaults: {
        bedCount: '3',
        bathCount: '3',
        sqft: '1'
    },

    schema: {

        firstName: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'First name'
            },
            title: null

        },
        lastName: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Last name'
            },
            title: null

        },

        searchRadius: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': '#',
                'class': 'searching'
            },
            title: null


        },
        zipcode: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': 'Zip code'
            },
            title: null
        },

        bedCount: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },

        bathCount: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },

        sqft: {
            validators: ['required'],
            editorAttrs: {
                'placeholder': ''
            },
            title: null
        },

        priceMax: {
            editorAttrs: {
                'placeholder': 'Max'
            },
            title: null
        },

        priceMin: {
            editorAttrs: {
                'placeholder': 'Min'
            },
            title: null
        },

        description: {
            editorAttrs: {
                'placeholder': "Discribe what you are looking for"
            },
            title: null
        },
        phoneNumber: {
            // validators: ['required'],
            editorAttrs: {
                'placeholder': 'Phone number'
            },
            title: null
        },

        displayEmail: {
          type: 'Radio', options: [{val: true, label: ''}, {val: 'false', label: ''}],
          title: '',
          editorAttrs: {
              'id': 'display_email'
          }
        },


        activelySearching: {
          type: 'Radio', options: [{val: true, label: ''}, {val: 'false', label: ''}],
          title: '',
          editorAttrs: {
              'id': 'actively_searching'
          }
        },

        mobile: {
            type: 'Hidden',

            editorAttrs: {
                'placeholder': 'Password'
            },
            title: null,
            value: false
        }

    },
    url: 'http://localhost:3001/dashboard'
})

