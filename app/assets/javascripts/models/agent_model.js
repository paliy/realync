Realync.Models.Agent = Backbone.Model.extend({
    initialize: function() {
        _.bindAll(this, 'foo');
    },
    foo: function(){
        alert("is not working...");
    },

    defaults: {
        username: '',
        avatar: '',
        lastName: '',
        firstName: '',
        location: ''
    },

    url: 'http://localhost:3001/dashboard'
});