Realync.Models.Message = Backbone.Model.extend({
    initialize: function() {
      _.bindAll(this, 'index');
      _.bindAll(this, 'index');
      message_model = this;
      this.set('pending', this.message_status())
      this.set('sender', this.sender())
      this.set('last_message', this.last_message())
      this.set('picURL', this.checkURL())

      this.set_data();
    },
    set_data: function(){
        if(this.get('type') == 'invitation'){
            var t_date = this.get('tourInfo')['startTime'];
           var date = this.get_day(t_date) + ' ' +
                this.get_month(t_date) + ' ' + new Date(t_date).getDate();
           this.set({start_date: date } );
           this.set({start_time: this.get_time(this.get('tourInfo')['startTime'])});
        }
    },

    message_status: function(){
        var users = this.get("users");
        var current_user = JSON.parse(localStorage.user)
        if(users){
            var status = _.filter(users, function(u){ return u.id == current_user.id })
            console.log(status);
            for(s in status){
                if(status[s].status == "pending"){
                    return "pending"
                }

            }
        }
        return "read"
    },

    get_minuts_ago: function (type) {
        var date = '';
        if(type == 'request'){
            date = this.get('createdAt');
        }else{
            var messages = this.get('messages');
            var last_messages = messages[messages.length - 1];

            date = last_messages['timeStamp'];
        }

        var c = ( new Date() - new Date(date))/1000;
        var month = date.split('T')[0].split('-')[1];
        var day = date.split('T')[0].split('-')[2];
        month = this.get_short_month(parseInt(month));
        var days = Math.floor(c/86400);
        c -= days * 86400
        var h = Math.floor(c/3600);
        c -= h*3600;
        var m = Math.floor(c/60);
        var result;
        if( days > 30 ){
            result = month + ' ' + day;
        }else if( days > 0){
            result = days + ' DAYS'
        }else if(h > 0){
            result = h + ' HRS'
        }else if(m > 0){
            result = m + ' MIN'
        }else{
            result = 'Less then minutes ago'
        }
        return result;
    },

    get_time: function(data){
        var d = new Date(data);
        var hour = '';
        var am = '';
        if(d.getHours() - 12 > 0){
            hour = d.getHours() - 12
            am = 'PM'
        }else{
            hour = d.getHours();
            am = 'AM'
        }
        return  hour + ':' + d.getMinutes() + ' ' +am;
    },
    get_month: function(data){
        var d = new Date(data);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var n = month[d.getMonth()];
        return n
    },

   get_short_month: function(num){
        var month = ["", "JAN", "FEB", "MAR",  "APR", "MAY", "JUNE", "JULE", "AUG", "SEPT", "OCT", "NOV",  "DEC"]
        var n = month[num];
        return n
    },

    get_day: function(data){
        var d = new Date(data);
        var week = [ "Sunday", "Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        var n = week[d.getDay()];
        return n
    },

    checkURL: function(){
        console.log('check url');
        var url = '';
        if(this.get('picURL')== "/images/noPhoto.jpeg"){
            url = localStorage.url + '/images/noPhoto.jpeg';
        }else{
            url = this.get('picURL');
        }
        return url
    },


    last_message: function(){
        var type = this.get('type');
        var last = '';
      if(type == 'message'){

        var messages = this.get('messages');
        var message_one = this.get('message');

        console.log(Array.isArray(message_one))
        console.log(message_one)
        if(messages){

            this.set('minAgo', this.get_minuts_ago())

            last = messages[messages.length -1]['message'];
        }else if(message_one){
            this.set('minAgo', this.get_minuts_ago('request'))
            last = this.get('message')['message'];
        }

      }else if(type == 'invitation'){
          this.set('minAgo', this.get_minuts_ago('request'));
          var tourInfo = this.get('tourInfo');
          if(this.get("sender") && !this.get("sender").status){
              last = tourInfo["address"] + " \n "
              last += this.get('message')
          }else{
              last = tourInfo["startTime"] + " \n "
              last += tourInfo["address"] + " \n "
              last += this.get('message')
          }


      }else if(type == "request"){
          this.set('minAgo', this.get_minuts_ago('request'))
          var current_user_id = JSON.parse(localStorage.profile_info).user.id
          var user = _.find(this.get("users"), function(user){return user.id == current_user_id })
          var arr = this.get("users")
          this.set("users", _.without(arr, user))
          last =  this.get("message")
      }

      return last;
    },

    sender: function(){
//       var users = this.get('users');

       var type = this.get('type');
       var users = this.get('users');
       var realtor = {};
       if(type == 'message'){
         var messages = this.get('messages');
         if(messages){
           realtor = _.find(this.get('users'), function(user){ return user['id'] == messages[messages.length -1]['sender'] });
         }else{
             realtor = {id: this.get('message')['message']['sender'],
                 name: "none",
                 picURL: "/images/noPhoto.jpeg"}
         }
       }else if(type == 'invitation'){
         var tourInfo = this.get('tourInfo');
         realtor = _.find(this.get('users'), function(user){ return user['id'] == tourInfo['realtorID'] });

       }else{
           realtor = {id: "none",
                      name: "none",
                      picURL: "/images/noPhoto.jpeg"}
       }
        if(realtor && realtor.picURL == "/images/noPhoto.jpeg"){
            realtor.picURL = localStorage.url + '/images/noPhoto.jpeg';
        }

       return realtor;
    },

    index: function (type) {

        $.ajax({
            url: localStorage.url + '/message/index',

            type: 'GET',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){

                console.log("Model index");
                console.log(output);
                message_model.add_messages_list(output.messages, type);


                if (type == 'dashboard') {
                    var view = new Realync.Views.agentRightbar;
                    view.addRecentMessageList();
                }
            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

    showMessage: function(message_id){

        $.ajax({
            url: localStorage.url + '/message/show/' + message_id,
            type: 'GET',

            xhrFields: {
                withCredentials: true
            },

            success: function(output, status, xhr){
                console.log('Ok');

                console.log(output);
            },
            error: function(data){
                console.log('Error');
            }

        })

    },

    delete_message: function (id) {
        $.ajax({
            url: localStorage.url + '/message/delete/' + id,

            type: 'POST',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log(output);

                var timeout_func = function(){
                    var index = new Realync.Views.Message_Index({model: this.current_user})
                    index.render();
                    var message = new Realync.Models.Message;
                    message.index();
                }
                setTimeout(timeout_func, 500);
//
            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

    accept_tour: function(id){
        $.ajax({
            url: localStorage.url + '/message/acceptTour/' + id,

            type: 'POST',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log('output');
                console.log(output);

            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

    decline_tour: function(id){
        $.ajax({
            url: localStorage.url + '/message/declineTour/' + id,

            type: 'POST',

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log(output);

            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

    reply_message: function (data, id, chat, position) {
        $.ajax({
            url: localStorage.url + '/message/reply/' + id,

            type: 'POST',

            data: data,

            xhrFields: {
                withCredentials: true
            },
            success: function(output, status, xhr){
                console.log(output);

                var timeout_func = function(){Backbone.history.navigate('#!/update_messages/' + id, { trigger: true });}
                setTimeout(timeout_func, 500);
            },
            error: function(data){
                console.log('Error update property');
            }

        })
    },

    add_messages_list: function(data, type){
        console.log('AAAAAAAAAAAAAAAAAADDDD');
        console.log(data);
        var collection = Realync.messages_collection;
        collection.reset();
        var current_user_id = JSON.parse(localStorage.profile_info).user.id;

        for(index in data){
            var invitation = _.find(data[index].users, function(user){return !user.status})
            var types = data[index].type;

            if((types == "invitation")){
                console.log(data[index])

                var tour = _.find(Realync.agent_tour_collection.models, function(t){return t.get("id") == data[index].tourInfo.id})

                if(tour){
                    console.log("999999999999999999999999999999999999999")
                    console.log("999999999999999999999999999999999999999")
                  message_model.delete_message(data[index].id)
                }else{
                    console.log("777777777777777777777777777777777777")
                    console.log("777777777777777777777777777777777777")
                    var mess = new Realync.Models.Message(data[index]);
                    collection.add(mess);
                }
            }else{
                var mess = new Realync.Models.Message(data[index]);
                collection.add(mess);
            }

        }

        var view = new Realync.Views.Message_list;
        view.render();
        if(type == "reply"){
            var message_i =  _.find(Realync.messages_collection.models, function(item){
                var arr = window.location.hash.split('/')
                return (item.get('id')) == arr[arr.length -1];
            });
            var view = new Realync.Views.Chat_message({model: message_i});
            view.render();
            Backbone.history.navigate('#!/messages', { trigger: false });
        }
    },

    url: 'http://localhost:3001/dashboard'
});