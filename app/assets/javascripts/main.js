//= require jquery
//= require jquery_ujs
//= require jquery.ui.all
//= require underscore
//= require backbone
//= require select2.js
//= require realync
//= require exif.js
//= require canvas_resize.js
//= require jquery.ui.widget.js
//= require jquery.iframe-transport.js
//= require jquery.fileupload.js
//= require load-image.min.js
//= require jquery.fileupload-process.js
//= require file_load_image.js
//= require file_upload.video.js

//= require backbone-forms
//= require foundation-datetimepicker.js
//= require_tree ./models
//= require_tree ./collections
//= require ../templates/context
//= require foundation
//= require foundation/foundation.orbit.js
//= require fullcalendar.js
//= require jquery.ui.datepicker
//= require jquery.datetimepicker.js
//= require braintree.js
//= require jquery.rs.carousel.js
//= require jquery.rs.carousel-continuous.js

$(document).ready(function(){
  $(document).foundation();
});
