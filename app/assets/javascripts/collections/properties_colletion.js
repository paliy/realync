Realync.Collections.Properties = Backbone.Collection.extend({
    initialize: function() {

       _.bindAll(this, "last_three");
       _.bindAll(this, "sort_by_address");
       _.bindAll(this, "sort_for_select");
       _.bindAll(this, "find_property_by_id");
       _.bindAll(this, "remove_property");
//        Realync.Collections.Tours .bind('add', this.foo);
    },
    
    last_three: function(){
        return this.models.slice(-3);
    },

    sort_by_address: function(){

    },

    sort_for_select: function(){
        var arr = [];
        for( i in this.models){
          var attr = this.models[i].attributes;
          if(!attr.picURL){
              attr.picURL = '/assets/property_no_image.png'
          }

          arr.push({ id: attr.id, text: attr.searchString, src: attr.picURL});
        };
        return arr
    },

    find_property_by_id: function(id) {
      console.log(id);
      var property = _.find(this.models, function(prop){ return prop.get('id') == id });
      console.log(property);
      return property;
    },

    remove_property: function(id) {
        var property = _.find(this.models, function(prop){ return prop.get('id') == id });
        property.destroy();
        return false;
    },

    getPropertyTours: function(id){
        var tours = Realync.agent_tour_collection.models;
        var property_tours = _.filter(tours, function(t){ return t.get('property').id == id });

        return property_tours;
    },

    model: Realync.Models.Property,

    
    url: function(){
        console.log('create properties');
    }

});

Realync.properties_collection = new Realync.Collections.Properties;