Realync.Collections.Messages = Backbone.Collection.extend({
    initialize: function() {

        _.bindAll(this, "last_three");
    },
    last_three: function(){
        return this.models.slice(-3).reverse();
    },

    model: Realync.Models.Messages,
    url: function(){
        console.log('create properties');
    }

});

Realync.messages_collection = new Realync.Collections.Messages();