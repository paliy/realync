/**
 * Created by entity on 4/1/14.
 */
/**
 * Created by entity on 4/1/14.
 */
//= require ./main
//= require_tree ../templates/dashboard
//= require_tree ./views/dashboard
//= require_tree ./routers/dashboard

//= require_tree ../templates/properties
//= require_tree ./views/properties


//= require_tree ../templates/sessions
//= require_tree ../templates/landing
//= require_tree ./views/sessions
//= require_tree ./views/landing


//= require_tree ../templates/forms
//= require_tree ./forms


//= require_tree ../templates/messages
//= require_tree ./views/messages

//= require_tree ../templates/tours
//= require_tree ./views/tours

//= require_tree ../templates/live_tour
//= require_tree ./views/live_tour

