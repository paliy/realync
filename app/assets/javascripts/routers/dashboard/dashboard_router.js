Realync.Routers.DashboardWrapper = Backbone.Router.extend({
    initialize: function(){
        console.log('init dashboard');
//        current_user = localStorage.user;
        this_dashboard = this;
        var rout = window.location.pathname;
        if(!localStorage.user && "/dashboard" == rout){
           window.location = '/landing'
        }


        var rout_hash = window.location.hash;

        if(rout_hash == ""){
            window.location = "#!/"
        };

        if(localStorage.user){

          this.current_user = new Realync.Models.User(JSON.parse(localStorage.user))
            current_user = new Realync.Models.User(JSON.parse(localStorage.user))
        }

        if(localStorage.profile_info){
          this.current_user_profile = new Realync.Models.Profile( JSON.parse(localStorage.profile_info));
          this.role = JSON.parse(localStorage.profile_info).user.realtor;
        }

        if(this.current_user && this.current_user.attributes.realtor){
            $('a[href="#!/agents"]').hide();
            console.log("11111111111111")
        }
    },
    routes: {
        '!/agentBrokers' : 'agentBrokers',
        '!/contactUs' : 'contactUs',
        '!/ourTeam' : 'ourTeam',
        '!/help' : 'help',
        '!/blog' : 'blog',
        '!/careers' : 'careers',
        '!/buyersRenters' : 'buyersRenters',
        '!/pricing' : 'pricing',
        "!/": "_new",
        "!/": "dashboard",
        "!/newProperty": "newProperty",
        // "!/editProperty": "editProperty",
        "!/viewProperty": "viewProperty",
        "!/properties": "properties",
        "!/createTour": "createTour",
        "!/agentProfile": "agentProfile",
        "!/clientProfile": "clientProfile",
        "!/myAgentProfile": "myAgentProfile",

        "!/upcoming": "upcoming",
        "!/past": "past",
        "!/tour/show/:id": "showTour",
        "!/tour/edit/:id": "editTour",

        "!/agents" : "agents",
        "!/messages": "messages",
        "!/update_messages/:id": "update_messages",
        "!/messages/:id": "messages_with_chat",
        "!/settings": "settings",
        "!/pay": "pay",
        "!/search": "search",
        "!/live_tour/:id" : "live_tour",

        "!/property/:id" : 'viewProperty',
        "!/property/edit/:id" : 'editProperty',
        "!/property/delete/:id" : 'deleteProperty',

        "!/settings/account": 'account',
        "!/settings/subscription": 'subscription',
        "!/settings/sharing": 'sharing',
        "!/settings/profile": 'profile',

        "!/profile" : "profile",
        "!/load" : "load_file",

        '!/calendar' : "calendar",
        "!/tour_list": "tour_list",
        "!/list": "list",
        "!/sign_up": "sign_up",
        "!/sign_in": "sign_in",
        "!/log_out": "log_out",
        "!/user/resetConfirm/:token" : "resetConfirm",
        "!/updateSubscription" : "updateSubscription",
        "!/updatePaymentMethod" : "updatePaymentMethod",

        '*filter' : 'login'
    },

    showTour: function(){
      console.log('show tour');
      var arr = window.location.hash.split('/');
      var id = arr[arr.length - 1];
      var tour = new Realync.Models.Tour;
      tour.showVideo(id);

    },

    editTour: function(){
        var tour = new Realync.Models.Tour;
        tour.agentTour();
      console.log('edit tour');
      var arr = window.location.hash.split('/');
      var id = arr[arr.length - 1];
      var tour = _.find(Realync.agent_tour_collection.models, function(tour){ return tour.get("id") == id});

      var view = new Realync.Views.EditTour({model: tour});
      view.render();

    },

    live_tour: function(){
        var arr = window.location.hash.split('/');
        var id = arr[arr.length - 1];
        var tour = {};
        tour = _.find(Realync.agent_tour_collection.models, function(tour_l){ return tour_l.get("id") == id});

        if(!tour){
            tour = _.find(Realync.upcoming_collection.models, function(tour_l){ return tour_l.get("id") == id});
       }
        console.log("OPENTOK");
        console.log(tour);
        var view = new Realync.Views.Opentok({model: tour});
        view.render();
        $('#property_name h1').html(tour.get('address').street1);
        view.info();
    },

    pay: function(){
        var view = new Realync.Views.Pay;

        view.render();
    },

    load_file: function(){
      var view = new Realync.Views.File_load();
     view.render();
    },

    log_out: function(){

      delete localStorage.user
      window.location = "landing#!/"
      this.current_user.logout();
      localStorage.clear();
    },

    resetConfirm: function(){
        console.log('Reset Confirm');
        var view = new Realync.Views.Session.ResetConfirm();
        $(this.el).html(view.render());

        return false;
    },

    sign_in: function(){
        console.log('Sign in');
        var view = new Realync.Views.Session.Login ();
        $(this.el).html(view.render());

        var session = new Realync.Models.Session({
        });

        var form = new Backbone.Form({
            template: _.template($('#sessionTemplate').html()),
            model: session

        }).render();

        form.on('submit', function(event) {

                $('.api-errors').html(''); //clear errors
                form.setValue({ mobile: false });

                var data = form.getValue();


                var errors = form.commit();
            console.log(errors);
                if(! errors){
                    session.login(data);
                }

        });

        $('#auth-page').html(form.el);
        $("#content-bb").remove();
        return false;
    },

    sign_up: function(){
        console.log('Sign up');

        var view = new Realync.Views.Session.SignUp();
        $(this.el).html(view.render());
        $("#content-bb").remove();
        return false;
    },

    agentBrokers: function(){
        console.log("agentBrokers");

       var view = new Realync.Views.agentBrokers();
       $(this.el).html(view.render());
       return false;
    },

    buyersRenters: function(){
        console.log("buyersRenters");

        var view = new Realync.Views.buyersRenters();
        $(this.el).html(view.render());

        return false;
    },

    careers: function(){
        console.log("Careers");

        var view = new Realync.Views.careers();
        $(this.el).html(view.render());
        return false;
    },

    account: function(){
        console.log("Account");

        var view = new Realync.Views.Settings({model: {role: this.current_user.attributes.realtor}});
        view.render();
        (new Realync.Views.Account()).render();
    },

    subscription: function() {
        console.log("Subscription");

        var view = new Realync.Views.Settings({model: {role: this.current_user_profile.attributes.user.realtor}});
        view.render();
        (new Realync.Views.Subscription({model: {role: this.current_user_profile.attributes.user.realtor}})).render();
    },

    sharing: function() {
        console.log("Sharing");

        var view = new Realync.Views.Settings({model: {role: this.current_user.attributes.realtor}});
        view.render();
        (new Realync.Views.Sharing()).render();
    },

    ourTeam: function(){
        console.log("OurTeam");

        var view = new Realync.Views.ourTeam();
        $(this.el).html(view.render());

        return false;
    },

    contactUs: function(){
        console.log("contactUs");

        var view = new Realync.Views.contactUs();
        $(this.el).html(view.render());

        return false;
    },

    pricing: function(){
        console.log("pricing");

       var view = new Realync.Views.Pricing();
        $(this.el).html(view.render());

       return false;
    },

    help: function(){
        console.log('Help');

        var view = new Realync.Views.Help();
        $(this.el).html(view.render());

        return false;
    },

    blog: function(){
        console.log("blog");

        var view = new Realync.Views.Blog();
        $(this.el).html(view.render()); 

        return false;
    },

    updateSubscription: function(){
        console.log("updateSubscription");

        var view = new Realync.Views.updateSubscription();
        $(this.el).html(view.render());
        return false;
    },

    updatePaymentMethod: function(){
        console.log("updatePaymentMethod");

        var view = new Realync.Views.updatePaymentMethod();
        $(this.el).html(view.render());
        return false;
    },

    _new: function () {
        (new Realync.Views.DashboardWrapper).render();
    },

    dashboard: function(){
        console.log("Upcoming Tour");
        (new Realync.Views.Properties_index).render();
        var upcoming_tour = new Realync.Models.Tour;
        upcoming_tour.upcomingTourAgent();
        upcoming_tour.agentPastTours();
        upcoming_tour.clientTours();
        (new Realync.Views.DashboardWrapper).render();

    },

    upcoming: function(){
        if(this.role == true){
            (new Realync.Views.Calendar).render();
            var tour = new Realync.Models.Tour;
            tour.agentTour();
            tour.agentPastTours();
        }
        else{
            (new Realync.Views.Upcoming).render();
            var tour = new Realync.Models.Tour;
        }


    },

    past: function(){
        console.log('Past Tours');

        (new Realync.Views.Past).render();
    },

    tour_list: function(){
        console.log("Tour list");
        (new Realync.Views.TourList).render();
    },

    agents: function() {
        console.log("Agents");
        if(!$.isEmptyObject(JSON.parse(localStorage.profile_info).myAgent)) {
            this.myAgentProfile();
        } else {
            this.current_user.findAgent();
        }
//        (new Realync.Views.Agents).render();
    },

    viewAgentList: function() {
        (new Realync.Views.Agents).render();
    },

    messages: function(){
        console.log('----render messages')
        var index = new Realync.Views.Message_Index({model: this.current_user})
        index.render();
        var message = new Realync.Models.Message;
        message.index();
    },

    messages_with_chat: function(){
        var arr = window.location.hash.split('/');
        var id = arr[arr.length - 1];

        var index = new Realync.Views.Message_Index({model: this.current_user})
        index.render(id);
        var message = new Realync.Models.Message;
        message.index();
    },

    settings: function(){
        (new Realync.Views.Settings).render();
    },


    newProperty: function(data) {
        var prop = new Realync.Models.Property({});
        var form = new Backbone.Form({
            template: _.template($('#propertyTemplate').html()),
            model: prop
        }).render();

        form.on('submit', function(event) {

            var data = form.getValue();
            var errors = form.commit();
            console.log(errors);
            console.log(current_user);
            if(! errors){
                prop.createProperty(data, current_user);
            }

        });

        var view = new Realync.Views.newProperty;
        view.render();

        $('#property').html(form.el);
        view.upload_multiple_images();
        view.upload_video();
    },

    editProperty: function(data) {
        console.log("Edit Property");
        var arr = window.location.hash.split('/');
        var id = arr[arr.length - 1];

        var prop = new Realync.Models.Property({});
        prop.getInfo(id, 'edit');
    },

    viewProperty: function() {
        console.log("View Property");
        var arr = window.location.hash.split('/');
        var id = arr[arr.length - 1];

        var prop = new Realync.Models.Property;
        prop.getInfo(id, 'view');
    },

    deleteProperty: function() {
        var arr = window.location.hash.split('/');
        var id = arr[arr.length - 1];

        var prop = new Realync.Models.Property({});
        prop.delete(id);
    },

    agentProfile: function() {
        var id = JSON.parse(localStorage.profile_info).myAgent.id;
        console.log("Agent profile");
        console.log(this.current_user.attributes);
        this.current_user.getAgentProfile(id);
    },

    clientProfile: function() {
        (new Realync.Views.ClientProfile).render();
    },

    myAgentProfile: function() {
        var id = JSON.parse(localStorage.profile_info).myAgent.id;
        console.log("myAgent profile");
        console.log(this.current_user.attributes);
        this.current_user.getAgentProfile(id);
    },

    search: function(){
        (new Realync.Views.SearchResult).render();
    },

    profile: function(){
        var isRealtor = this.current_user_profile.attributes.user.realtor

        if(this.current_user && isRealtor){
          template = '#agentProfileTemplate';
        }else{
            var template = '#clientProfileTemplate';
        }

        var form = new Backbone.Form({
            template: _.template($(template).html()),
            model: this.current_user_profile

        }).render();

        (new Realync.Views.Profile).render(this.current_user_profile);
        $('.profile-container').append(form.el);

        if(isRealtor){


            // switch buttons displayEmail
            $("#switch_content .switch").html($($("ul[name='displayEmail'] li")[0]).html());
            $("#switch_content .switch").append($($("ul[name='displayEmail'] li")[1]).html());
            $("ul[name='displayEmail']").html("");
            $("ul[name='displayEmail'] input").prop('checked', false);
            $('input#c12_description').attr('placeholder','Tell your clients a little more about yourself');
            
            if(this.current_user_profile.attributes.displayEmail) {
                $('#switch_content input').first().prop('checked', true);
                $('#switch_content .switch').css('border', '2px solid #1dcb9a');
            } else {
                $('#switch_content input').last().prop('checked', true);
                $('#switch_content .switch').css('border', '2px solid #38414e');
            }
            form.on('submit', function(event) {
                var data = form.getValue();
                var errors = form.commit();
                data.displayEmail = $("#switch_content .switch input:checked").val();
                if(!( errors.hasOwnProperty("zipcode")||errors.hasOwnProperty("firstName")||
                    errors.hasOwnProperty("description")||errors.hasOwnProperty("lastName")
                    ||errors.hasOwnProperty("phoneNumber"))){
                    current_user.get_params_by_zip(data, 'agent');
                    $('.btn.btn-primary').click()
                }

            });
        }else{
            $("#switch_content .switch").html($($("ul[name='activelySearching'] li")[0]).html());
            $("#switch_content .switch").append($($("ul[name='activelySearching'] li")[1]).html());
            $("ul[name='activelySearching']").html("");
            $("ul[name='activelySearching'] input").prop('checked', false);
            
            if(this.current_user_profile.attributes.activelySearching) {
                $('#switch_content input').first().prop('checked', true);
                $('#switch_content .switch').css('border', '2px solid #1dcb9a');
            } else {
                $('#switch_content input').last().prop('checked', true);
                $('#switch_content .switch').css('border', '2px solid #38414e');
            }

            form.on('submit', function(event) {
                var data = form.getValue();
                var errors = form.commit();
                data.activelySearching = $("#switch_content .switch input:checked").val();
                if(! errors){
                    current_user.get_params_by_zip(data, 'client');
                }
                $('.btn.btn-primary').click()

            });
        }

        $('.slider-bedrooms').foundation('slider', 'set_value', this.current_user_profile.attributes.bedCount);
        $('.slider-bathrooms').foundation('slider', 'set_value', this.current_user_profile.attributes.bathcount);
        $('.slider-sqft').foundation('slider', 'set_value', this.current_user_profile.attributes.sqft);

        $('[data-slider]').on('change', function(){
            $(".slider-bedrooms input[name='bedCount']").val($('.slider-bedrooms [data-slider]').attr('data-slider'));
            $(".slider-bathrooms input[name='bathCount']").val($('.slider-bathrooms [data-slider]').attr('data-slider'))
            $(".slider-sqft input[name='sqft']").val($('.slider-sqft [data-slider]').attr('data-slider'))
        });

        $(document).foundation();
    },


    calendar: function() {
        var view = new Realync.Views.Calendar;
        view.render();
    },

    properties: function() {
        console.log("Properties");
        (new Realync.Views.Properties_index).render();
        var property = new Realync.Models.Property;
        property.index('property_index');
        // (new Realync.Views.Properties).render();
    },

    update_messages: function(){
        var message = new Realync.Models.Message;
        message.index("reply");


    },

    createTour: function() {
        console.log("create Tour");

        var tour = new Realync.Models.Tour({});
        var form = new Backbone.Form({
            template: _.template($('#tourTemplate').html()),
            model: tour
        }).render();


        form.on('submit', function(event) {
            console.log($($("[data-fields='propertyID'] input")[0]).val())
            var data = form.getValue();
            if(data.propertyID.split(" ").length > 1){
                data.addressString = data.propertyID;
                delete data.propertyID;
            }

            var errors = form.commit();
            console.log(errors);
            data.clients = add_attendees();
            var arr = data.dateTime.split("  ");
            var min = new Date($.now()).getTimezoneOffset();
            data.dateTime = arr[0] + " " + arr[arr.length -1] + tour.getHours(min);
            data.isOpenHouse = $("input[name = 'tour']:checked").val();
            data.userID = current_user.id;



            console.log(errors)
            console.log(data)
            if(!errors || (data.isOpenHouse == "true" && Object.keys(errors).length == 1)){
                if(data.isOpenHouse == "true"){
                    data.shareByEmail = "true"
                }
                tour.createTour(data);
            }

        });

        function add_attendees(){
            var arr = '[';
            var i = 1;
            var user_container = $('div.attendees-container div div.attendee-wrap' )
            user_container.each(function(){
                arr += '{                                         \
                  "id": "' + $(this).find(' .user-id div').attr('id') + '",\
                  "name": "' + $.trim($(this).find('.user-name').text()) + '",\
                  "picURL": "' + $(this).find('img').attr('src') +'"\
                }';

                if(i < user_container.length){ arr += ','; }
                i +=1;
            });
            arr += ']';
            console.log(arr);
            return arr;
        };

        (new Realync.Views.CreateTour).render();
        $('#tour').html(form.el);
        $('#calendar_div').datetimepicker({
            format: 'D M d Y                                      H:i',
            onChangeDateTime: function(current_time, $input) {
                $('input[name="dateTime"]').val(current_time.dateFormat('D M d Y                                    H:i'));
                if ($('input[name="dateTime"]').val() != "") {
                    $(".dateTime_place").css({"display":"block"});
                }
            },
            step:5
        });


        $("input[name='propertyID']").click(function() {
            console.log("propertyID");
            if ($(".property_in_form").val() == ""){
                $(".property_place").css({"display":"block"});
                console.log("Property in form");
            } else if ($(".property_in_form").val() == undefined) {
                $('.property_place').css({"display":"none"});
                console.log("Property not in form");
            }
        });

        var arr = Realync.properties_collection.sort_for_select();

        function format(state) {
            console.log(state);
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='" + state.src + "'/>"  + "<div class='property_in_form'>"+ state.text +"</div>" ;
        }

        var property_for_schedule = {};
        if(localStorage.property_schedule_id){
            property_for_schedule = _.find(arr, function(element){return element.id == localStorage.property_schedule_id});
        }

        $('input[name="propertyID"]').select2({
            multiple: true,
            maximumSelectionSize: 1,
            formatResult: format,
            formatSelection: format,
            tags: arr,

            initSelection: function (element, callback) {
                var data = property_for_schedule;
                callback(data);
            }

        });



        if(localStorage.property_schedule_id){
            $('input[name="propertyID"]').select2('val', 193);

            delete localStorage.property_schedule_id;
        }

        $('input[name="attendees"]').select2({
            multiple: true,
            placeholder: "Search for a movie",
            minimumInputLength: 1,
            formatInputTooShort: customSign,
            ajax: {
                url: localStorage.url + '/user/searchUserList',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                crossDomain: true,
                dataType: 'json',


                data: function (term, page) {
                    return {
                        searchTerm: term
                    };
                },
                results: function (data, page) {
                    return {results: data.users};
                }
            },
            formatResult: movieFormatResult,
            formatSelection: movieFormatSelection,
            dropdownCssClass: "bigdrop",
            formatNoMatches: formatNotFound,
            escapeMarkup: function (m) { return m; }
        })

        function customSign() {
            return '<div class="search_div">Search for a person in Realync by name or email address,' +
                '       or enter an email address to invite someone new.</div>'
        };
        function formatNotFound(term) {
            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            if(validateEmail(term)){
                function inviteUserClick(){
                    $("#invite_user_by_email").click(function(){
                        var full_name = $("#invite-user-full-name").val();
                        if(full_name){
                            var tour = new Realync.Models.Tour

                            tour.inviteUserForTour(term, full_name);
                        }
                        $('input[name="attendees"]').select2("close")
                    })
                }
                var context =  "<div class='search_div'>" + "<b>" + term + "</b>" + '? We don`t know that person. Add their name and select "Invite User". '
                    + 'We`ll send them an invite email. Once they sign up, you`ll be added as their agent and they`ll recieve an invite to access your tour. </br></br></br>'

                    + "<div class='full_name'><b> FULL NAME </b>" +
                    "<input class='full-name' id='invite-user-full-name' type='text'/>"+
                    "</div>" +
                    "<input type='submit' class='button' id='invite_user_by_email' value='Invite User'>"
                    + "</div>";


                setTimeout(inviteUserClick, 500);
                return context;
            }else {
                return  "<div class='search_div'>" + "We`re sorry. There are no results for " + "<b>"+ term +"</b>" + "</div>"
            }


        }
        function movieFormatResult(user) {
            var current_user = new Realync.Models.User(JSON.parse(localStorage.user));

            var markup = "<div class='movie-result'>";

            if(user.mobilePicUrl == "/images/noPhoto.jpeg"){
                user.mobilePicUrl = localStorage.url + "/images/noPhoto.jpeg";
            }
            markup += "<div class='user-avata'><img class='round' src='" + user.mobilePicUrl + "'/></div>";
            markup += "<div class='user-name'><div>" + user.name + "</div></div>";
            markup += "</div>";
            return markup;

        }
        function movieFormatSelection(user) {
            var li = '<div>';
            li += "<div class='movie-result'>";
            li += "<div class='attendee-wrap'>";
            li += "<img class='round' src='" + user.mobilePicUrl + "'/>";
            li += "<div class='user-name' >" + user.name + "</div>";
            li += "<div class='user-id'><div style='visibility:hidden' id='" + user.id + "'></div></div>";
            li += "</div>";
            li += "<div class='remove-attendee' id='" + user.id + "'><img class='remove-icon' src='" + "assets/remove-attendee.png" + "'/></div>";
            li += '</div>';

            $('div.attendees-container').append(li);

            $('.remove-attendee').click(function(){
                var attendees = $('input[name="attendees"]').val();
                var spl = attendees.split(',');
                var user_id = $(this).attr('id');
                spl.forEach(function(val) {
                    if(val == user_id) {
                        spl.splice(spl.indexOf(val), 1);
                    }
                })
                $('input[name="attendees"]').val(spl.join(','));
                $(this).parent('.movie-result').remove();
            });

        }
    },
    // Properties
    list: function () {
        console.log('property list');
    }

});

var dashboardController = new Realync.Routers.DashboardWrapper();
