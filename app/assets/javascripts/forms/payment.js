
Realync.PaymentForm = Backbone.Form.extend({

    template: _.template($('#paymentTemplate, #paymentSettingsTemplate').html()),


    schema: {
        postal_code: {
            validators: ['required'],
            title: null,
            editorAttrs: {
                'placeholder': 'Postal Code'
            }
        },
        last_name: {
            validators: ['required'],
            title: null,
            editorAttrs: {
                'placeholder': 'Last Name'
            }
        },

        number: {
            validators: ['required'],
           title: null,
//           type: 'Number',
           editorAttrs: {
               'placeholder': 'Card Number',
               'data-encrypted-name': "number"
           }
        },
        cvv: {
            validators: ['required'],
           title: null,
           type: 'Number',
           editorAttrs: {
               'placeholder': 'Security Code',
               'data-encrypted-name': "cvv"
           }
        },
        month: {
            validators: ['required'],
            title: null,
            type: 'Select',
            editorAttrs: {
                'data-encrypted-name': "month"
            },

            options: [{
                        val: null,
                        label: 'Expiration Month'
                      }, 
                      '01', '02', '03',
                      '04', '05', '06', '07', '08',
                      '09', '10', '11', '12']
        },
        year: {
            validators: ['required'],
            type: 'Select',
            title: null,
            editorAttrs: {
                'data-encrypted-name': "year"
            },
            options: expirationYear()
        },

        plan:{
            type: "Hidden"
        }



    }

});

function expirationYear() {
  var years = [];
  var current_year = new Date().getFullYear();
  years[0] = { val: null, label: 'Expiration Year' };
  years[1] = current_year;
  for (var i = 1; i < 16; i++) {
    years[i+1] = current_year + i;
  };
  // console.log(years);
  return years;
}
