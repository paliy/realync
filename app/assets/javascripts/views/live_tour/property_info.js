Realync.Views.PropertyInfo = Backbone.View.extend({

    template: JST['live_tour/propertyinfo'],
    tagName: 'div',

    render: function () {
        console.log("Property info");
        console.log(this.model);
        this.$el.html(this.template(this.model));
        return this;
    }
});