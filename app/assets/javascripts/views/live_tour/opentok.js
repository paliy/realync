Realync.Views.Opentok = Backbone.View.extend({
    initialize: function() {
        opentok_view = this;
    },
    template: JST['live_tour/opentok'],
    el: "#page-content-bb",

    events: {
        "click .mute" : "mute",
        "click .audio" : "audio",
        "click .notes" : "notes",
        "click .info" : "info",
        "click .chat" : "chat"
    },

    rating: function(){
        $('#rate_tour').height($('body').height());
        $('.rate_tour_container').show();
        var view = new Realync.Views.RateTour({model: this.model});

        $('.rate_tour_container').html(view.render().el)
    },

    end_tour: function(){
        return false;
    },

    start_archiving: function(){
        var videoSession = this.model.get("videoSession");
        var tour_id = this.model.get("id");
        this.model.startArchiving(videoSession, tour_id);
    },

    get_minuts_ago: function (time_start, time_end) {
        var c = ( time_end - time_start)/1000;
        var h = Math.floor(c/3600);
        c -= h*3600;
        var m = Math.floor(c/60);
        return (h < 10 ? ('0' + h) : h) + ':' + (m < 10 ? ('0' + m) : m)
    },


    detect_realtor: function(fullname){

        var realtor_id = this.model.get("realtor");
        var user = _.filter(this.model.get("accessList"), function(u){return u.id == realtor_id});
        return fullname == user[0].name;
    },

    stop_archiving: function(session, time_start, time_end){
        var data = {};
        data["videoID"] = this.model.get("video");
        data["current_user_id"] = JSON.parse(localStorage.user).id;
        data["propertyID"] = this.model.get("property").id;
        data["archiveID"] = localStorage.arhivingID;
        data["runtime"] = (time_end - time_start)/1000;
        data["videoSession"] = this.model.get("videoSession");

       var save_data = {};
        save_data["tourID"] = this.model.get("id");
        save_data["videoID"] = this.model.get("video");
        save_data["chatArray"] = this.create_chat_array();
        save_data["notesArray"] = this.create_note_array();


        this.model.stopArchiving(this.model, session, data, save_data);
    },

    save_client_tour: function(){

        var save_data = {};
        save_data["tourID"] = this.model.get("id");
        save_data["videoID"] = this.model.get("video");
        save_data["chatArray"] = this.create_chat_array();
        save_data["notesArray"] = this.create_note_array();

        var agent = opentok_view.detect_realtor(JSON.parse(localStorage.profile_info).user.name);
        this.model.tourSave(save_data, agent);
    },

    create_chat_array: function(){
        arr = [];
        Realync.live_chat_item_collection.each(function(el){
            arr.push(JSON.stringify( { message:el.get('message'), timestamp: el.get('timestamp'), user: el.get('sender')[0].id}) );
        }, this)
        var result = '['
        for(i in arr){
            result += arr[i];
            if(i != arr.length-1){
                result += ','
            }
        };
        result += ']';
        Realync.live_chat_item_collection.reset();
        return result;
    },

    create_note_array: function(){
        var arr = [];
        Realync.my_notes_collection.each(function(el){
            arr.push(JSON.stringify( { note:el.get('note'), timestamp: el.get('timestamp')}) );
        }, this)
        var result = '['
        for(i in arr){
            result += arr[i];
            if(i != arr.length-1){
                result += ','
            }
        };
        result += ']';
        Realync.my_notes_collection.reset();
        return result;
    },

    mute: function(){
        console.log("Mute");

        $("#videoSectionclient .OT_mute").click();
        var el = $('.left .control-action div.mute');
        if(el.hasClass('mute-off')) {
            el.removeClass('mute-off').addClass('mute-on');
        } else {
            el.removeClass('mute-on').addClass('mute-off');
        }                                      
        return false;
    },

    audio: function(){
        console.log("audio");

        $("#videoSection .OT_mute").click();
        var el = $('.left .control-action div.audio');
        if(el.hasClass('audio-on')) {
            el.removeClass('audio-on').addClass('audio-off');
        } else {
            el.removeClass('audio-off').addClass('audio-on');
        }
        return false;
    },

    notes: function(){
        console.log("notes");
        var view = new Realync.Views.Notes();
        $("#video_section").width("75%");
        $('#videoSectionclient').width('80%').height('540px');
        $("#right-bar-live-tour").width("25%").show();
        $("#right-bar-live-tour").html(view.render().el);
        view.view_all_notes();
        this.close_rightbar();
        $('#right-bar-live-tour').animate({"scrollTop": $('#right-bar-live-tour')[0].scrollHeight});
        return false;
    },

    info: function(){
        console.log("info");
        var property = this.model.get("property");
        var view = new Realync.Views.PropertyInfo({model: property});
        $("#video_section").width("75%");
        $('#videoSectionclient').width('80%').height('540px');
        $("#right-bar-live-tour").width("25%").show();
        $("#right-bar-live-tour").html(view.render().el);
        this.close_rightbar();

        return false;
    },

    chat: function(){
        console.log("chat");
        var view = new Realync.Views.LiveChat();
        $("#video_section").width("75%");
        $('#videoSectionclient').width('80%').height('540px');
        $("#right-bar-live-tour").width("25%").show();
        $("#right-bar-live-tour").html(view.render().el);
        view.view_all_chat();
        this.close_rightbar();
        $('#right-bar-live-tour').animate({"scrollTop": $('#right-bar-live-tour')[0].scrollHeight});

        return false;
    },

    createChatItem: function(chat){
        var view = new Realync.Views.LiveChatItem({model: chat})
        $(".chatbody").append(view.render().el);
    },

    addMessageChat: function(name, data){
        var sender = _.filter(this.model.get("accessList"), function(u){ return u.name == name})
        var current_user = JSON.parse(localStorage.profile_info).user
        if(sender.length == 0){
            sender = _.filter(this.model.get("accessList"), function(u){ return u.id == current_user.id})
            sender[0]['realtor'] = true;
        }
        console.log(sender);
        var chat = new Realync.Models.LiveTourChat({sender: sender, message: data, timestamp: new Date().toISOString()});
        var live_chat = Realync.live_chat_item_collection;
        live_chat.add(chat);
        $(".chatbody").html("");
        live_chat.each(this.createChatItem, this)
    },

    createTourOpentok: function(videosession){
        Realync.live_chat_item_collection.reset();
        Realync.my_notes_collection.reset();
        var serverURL,tourDataString;
        var session;
        var sessionID = videosession;
        var apikey;
        var token;
        var username = "TTTTTTTTTTTTTT";
        var userFullName = JSON.parse(localStorage.profile_info).user.name;
        var senderName;
        var senderMessage;
        var publisher;
        var subscriberTable={};
        var time_start;
        subscriber={};
        var current_user = JSON.parse(localStorage.profile_info).user;

        $.ajax({
            type: "GET",
            url:  localStorage.url + "/tour/createRealtorToken/"+sessionID,
            dataType: "json",
            async:true,
            xhrFields: {
                withCredentials: true
            },
            success: function(response) { // on success..
                console.log('creating session');
                session = TB.initSession(sessionID);
                apikey = response.apiKey;
                token = response.token;
                console.log('connecting apikey =' + apikey + 'token = '+token);
                session.connect(apikey, token);
                session.addEventListener("signal", signalHandler);
                session.addEventListener("sessionConnected", sessionConnectedHandler);
                session.addEventListener("streamCreated", streamCreatedHandler);

            },
            error: function(response) {
                $('.errorMessage').html("<h2>No stored tours found for user</h2>");
            }

        });



          function sessionConnectedHandler (event) {
            subscribeToStreams(event.streams);
            console.log('publisher initialized?');
            if (!publisher) {
                console.log('No');
                var publisherProperties = new Object();
                publisherProperties.publishAudio= true;
                publisherProperties.name = userFullName;
                //divProps.name = username;
                var newID = 'audioR'+session.connection.connectionId;


                var agent = opentok_view.detect_realtor(current_user.name);

                if(agent){
                    $('#videoSectionclient').append('<div id="'+newID+'" class="realtorVideo"></div>');
                    publisherProperties.publishVideo = true;
                    opentok_view.start_archiving();
                    time_start = new Date();


                }else{
                    $('#videoSectionclient').append('<div id="'+newID+'" class="clientAudio"></div>');
                    publisherProperties.publishVideo = false;

                }
                publisher = TB.initPublisher(apikey,newID,publisherProperties);

                if(agent){
                    subscriber['video'] = publisher;
                }
                    publisher.hasAudio = true;
                    session.publish(publisher);

                    $('#publisher').hide();
                }
          }

          function subscribeToStreams(streams) {

            for (var i = 0; i < streams.length; i++) {
              var stream = streams[i];
              if (stream.connection.connectionId != session.connection.connectionId) {
                if(stream.hasVideo){
                   var divProps = {};
                   var newID = 'video'+session.connection.connectionId;
                   var agent = opentok_view.detect_realtor(stream.name);
                   if(agent){
                     time_start = new Date();
                     divProps = {height: "100%", width: "100%"};
                    
                   }
                   $('#videoSection').append('<div id="'+newID+'" class="realtorVideo"></div>')
                   subscriberTable[stream.connection.connectionId] = stream.name;

                    subscriber['video'] = session.subscribe(stream,newID,divProps);
                }else{
                    var newID = 'audio'+session.connection.connectionId;
                    subscriberTable[stream.connection.connectionId] = stream.name;
                    $('#videoSection').append('<div id="'+newID+'" class="clientAudio"></div>')
                    session.subscribe(stream,newID);
                }
              }else{
                if(stream.hasVideo){
                    var divProps = {height:430, width:490};
                    var newID = 'video'+session.connection.connectionId;
                    $('#videoSection').append('<div id="'+newID+'" class="realtorVideo"></div>')
                    console.log('Name of video stream - '+stream.name);
                    subscriberTable[stream.connection.connectionId] = stream.name;
                    session.subscribe(stream,newID,divProps);
                }else{
                    var newID = 'audio'+session.connection.connectionId;
                    $('#videoSection').append('<div id="'+newID+'" class="clientAudio"></div>');
                    session.subscribe(stream,newID);
                }
              }
            }

            $(".OT_subscriber").hide();
            var agent = opentok_view.detect_realtor(current_user.name);
            if(!agent){
              $('#videoSectionclient.false').css({'width': '0', 'height' : '0', 'opacity':'0'});
              $("#videoSection").css({
                  'width' : '80%',
                  'height': '72%',
                  'position' : 'absolute',
                  'top' : 0,
                  'bottom' : 0,
                  'right' : 0,
                  'left' : 0,
                  'margin' : 'auto',
                  'margin-top' : '80px'
              });
            }else{
              $("#videoSectionclient ").css({
                  'width' : '80%',
                  'height': '72%',
                  'position' : 'absolute',
                  'top' : 0,
                  'bottom' : 0,
                  'right' : 0,
                  'left' : 0,
                  'margin' : 'auto',
                  'margin-top' : '80px'
              });
            }

            $("#videoSection .OT_subscriber ").show();
            $("#videoSection .OT_video-container").show();
            $('.end_tour').off();
            $('.end_tour').click(function(){
                var agent = opentok_view.detect_realtor(current_user.name);
                if (agent){
                    var time_end = new Date();
                    opentok_view.stop_archiving(session, time_start, time_end);
                }else{
                    opentok_view.save_client_tour();
                    opentok_view.rating()
                    session.disconnect();
                }

                return false;
            });
          }

          function streamCreatedHandler(event) {
                subscribeToStreams(event.streams);
          };

          $('.chatSendButton').off();
          $(document).on('click', '.chatSendButton', function(){
            if($('.chatMessageBody').val() != "") {
                sendMessage();
                buildChatMessage('local');
            }
            return false;
          })
          $('.take_photo').off();
          $(document).on('click', '.take_photo', function(){
             var imgData = subscriber['video'].getImgData();
             window.open("data:image/octet-stream;base64," + imgData, "_blank");
             return false;
          });

          $('.chatMessageBody').bind('keypress', function (e){
             if(e.keyCode == 13) {
                sendMessage();
                buildChatMessage('local');
             }
             return true;
          });

          function sendMessage(){
             session.signal({
               data: current_user.id + '##' + $('.chatMessageBody').val(),
               replyTo: username,
               type: "message"
               },
               function(error) {
                 if (error) {
                    console.log("signal error ("
                    + error.code
                    + "): " + error.reason);
                 } else {
                    console.log("signal sent.");
                 }
               }
             );
                $('.chatMessageBody').val('');
                $('#right-bar-live-tour').animate({"scrollTop": $('#right-bar-live-tour')[0].scrollHeight}, "slow");
          }

          function buildChatMessage(origin){
            var chatUsername;
            var chatMessage;
            if(origin === 'received'){
                chatUsername = senderName;
                chatMessage = senderMessage;

            }else{
                chatUsername = username;
                chatMessage = $('.chatMessageBody').val();
            }

            var chatDiv = ' <div class="chatMessageTile">';
            chatDiv += '		<div class="chatUserTitle">'+chatUsername+':</div>';
            chatDiv += '		<div class="chatMessageText">'+chatMessage+'</div>';
            chatDiv += '	</div>';
            $('.chatbody').append(chatDiv);
            $('.chatMessageBody').val('');
          }

          function buildChatMessage(event){
          }

          function signalHandler(event){

            senderName = subscriberTable[event.from.id];
            senderMessage = event.data;
              console.log(event)
              console.log(event.data)
            opentok_view.addMessageChat(senderName, event.data);
            // If you receive message
            if(event.from.id != session.connection.connectionId){

                buildChatMessage('received');
            }
          }
    },

    close_rightbar: function() {
        $('.close_rightbar').click(function() {
            $('#right-bar-live-tour').hide();
            $('.right .control-action div').removeClass('active-btn');
            $('#video_section').width('100%');
            $('#videoSectionclient').width('62%').height('73%');
        })
    },

    render: function () {
        var role = JSON.parse(localStorage.profile_info).user.realtor
        console.log("OPENTOK VIEW");
        $(this.el).html(this.template({agent: role}));
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $('.right .control-action div').click(function(){
            $('.right .control-action div').removeClass('active-btn');
            $(this).addClass('active-btn');
        });
        console.log(this.model);
        this.createTourOpentok(this.model.get("videoSession"));
    }
});




