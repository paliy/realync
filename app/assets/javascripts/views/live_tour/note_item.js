Realync.Views.NoteItem = Backbone.View.extend({

    template: JST['live_tour/note_item'],
    tagName: 'div class="note_item"',

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
})