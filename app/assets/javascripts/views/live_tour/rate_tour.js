Realync.Views.RateTour = Backbone.View.extend({

    template: JST['live_tour/rate_tour'],
    tagName: 'div',

    events:{

      "click .add_rate" : "add_rate",
      "click .do_not_rate" : "do_not_rate"
    },

    add_rate: function(){
      if($('input[name="rating"]:checked').val()) {
        $('#rate_tour').hide();
        $('.rate_tour_container').hide();
        console.log("Add rate");
        var rate =  $('input[name="rating"]:checked').val();
        this.model.rateTour(this.model.get("video"), rate);
        window.location = "dashboard#!/"
      } else {
        alert('rating not set');
      }

    },

    do_not_rate: function() {
      $('#rate_tour').hide();
      $('.rate_tour_container').hide();
      window.location = "dashboard#!/";
    },

    render: function () {
        console.log("Rate");
        this.$el.html(this.template(this.model));
        return this;
    }
});