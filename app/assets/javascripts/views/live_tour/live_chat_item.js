Realync.Views.LiveChatItem = Backbone.View.extend({

    template: JST['live_tour/livechat_item'],
    tagName: 'div class=chat-item',

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});