Realync.Views.Notes = Backbone.View.extend({

    template: JST['live_tour/notes'],
    tagName: 'div',

    events: {
        "click .send_note" : "send_note"
    },

    send_note: function(){
        console.log("add note");
        var note_value = $(".note").val();

        if(note_value){
            var date = new Date().toISOString();
            var note = new Realync.Models.Notes({timestamp: date,
                                                 note: note_value})
            console.log(note);
            Realync.my_notes_collection.add(note);
            this.view_all_notes();
            $(".note").val('');
            $('#right-bar-live-tour').animate({"scrollTop": $('#right-bar-live-tour')[0].scrollHeight}, "slow");
        }
    },

    addNoteItem: function(note){
        var view = new Realync.Views.NoteItem({model: note});
        $("#notes").append(view.render().el);
    },

    view_all_notes: function(){
        $("#notes").html("");
        Realync.my_notes_collection.each(this.addNoteItem, this)
    },

    render: function () {
        console.log("Live notes");
        console.log(this.model);
        this.$el.html(this.template(this.model));
        return this;
    }
});