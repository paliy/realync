/**
 * Created by vitaliyjorzh on 10.04.14.
 */
Realync.Views.buyersRenters = Backbone.View.extend({

    template: JST['landing/buyersRenters'],
    el: "#content-bb",


    render: function () {
        $(".landing-image1, .landing-template1").hide(); // Temporarily
        this.$el.html(this.template(this.template()));
        $('.footer-links a, .top-bar-landing-links a').removeClass('active_link');
        $('a.buyers_link').addClass('active_link');
        return this;
       
    }
});