/**
 * Created by vitaliyjorzh on 11.04.14.
 */
Realync.Views.Help = Backbone.View.extend({

    template: JST['landing/help'],
    el: "#content-bb",


    render: function () {
    	$(".landing-image1, .landing-template1").remove(); // Temporarily
        this.$el.html(this.template(this.template()));
        $('.footer-links a, .top-bar-landing-links a').removeClass('active_link');
        $('a.help_link').addClass('active_link');
        return this;
    }
});