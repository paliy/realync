Realync.Views.updateSubscription = Backbone.View.extend({

    initialize: function(){
        this.current_user = new Realync.Models.User(JSON.parse(localStorage.user))
    },

    template: JST['sessions/updateSubscription'],
    el: "#login-bb",

    events: {
        "click #basic":'basic_subscription',
        "click #plus":'plus_subscription',
        "click #premium":'premium_subscription'
    },

    basic_subscription: function(){
//        var user = new Realync.Models.User({
//        });
//        var data = JSON.parse(localStorage.data_wait);
//
//        user.sign_up(data, true);
        console.log(this.current_user)
        var data = { plan: 'Trial', cardholderName: '', month: '', year: '', number: '', cvv: '' }
        this.current_user.startSubscription(data);
//        this.current_user.subscription('Basic');
        window.location = "/dashboard#!/"

        return false;


    },
    plus_subscription: function(){
        var view = new Realync.Views.updatePaymentMethod({model: {type: "Plus"}});
        view.render();
        //window.location.hash = "!/updatePaymentMethod"
        return false;
    },

    premium_subscription: function(){
        var view = new Realync.Views.updatePaymentMethod({model: {type: "Premium"}});
        view.render();
        //window.location.hash = "!/updatePaymentMethod"
        return false;
    },

    render: function () {
    	$("#content-bb").remove();
    	this.$el.html(this.template(this.template()));
        return this;
    }
});