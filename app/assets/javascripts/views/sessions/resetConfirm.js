Realync.Views.Session.ResetConfirm = Backbone.View.extend({

    template: JST['sessions/reset_confirm'],
    el: $("#login-bb"),

    events: {
        'click #confirmReset' : "confirmReset"
    },

    confirmReset: function(){
        console.log("Confirm")
        var user = new Realync.Models.Session
        var arr = window.location.hash.split("/")
        var token = arr[arr.length -1]
        user.resetConfirmation(token)
    },

    render: function () {
        console.log("render reset confirm")
        $(this.el).html(this.template());
        $(".title-text").hide();
    }
})