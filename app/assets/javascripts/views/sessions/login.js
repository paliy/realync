if( !Realync.Views.Session ){
    Realync.Views.Session = {};
}
Realync.Views.Session.Login = Backbone.View.extend({

    template: JST['sessions/login'],
    el: $("#login-bb"),

    events: {
        "click button": "check",
        "click a[href='#!/sign_up']": "sign_up",
        "click #button_forgotPass" : "forgotPassword",
        "click #button_resetPass" : "resetPass",
        "click #forgotPassBackButton" : "back_button"
    },

    resetPass: function(){
        console.log("Reset pass")
        var user = new Realync.Models.Session
        var username = $("#forgotPasswordUsername").val();
        user.resetPassword(username)

    },

    forgotPassword: function(){
        $("#forgotPasswordUsername, #button_resetPass, #forgotPassBackButton").show();
        $("#sign_in_field, #sign_in, #button_forgotPass").hide();
    },

    back_button: function(){
        $("#forgotPasswordUsername, #button_resetPass, #forgotPassBackButton").hide();
        $("#sign_in_field, #sign_in, .button_resetPass").show();
    },

    check: function () {
        console.log('tada');
    },
    sign_up: function(){

        var view = new Realync.Views.Session.SignUp();
        window.location.hash = "#!/sign_up"

        $(this.el).html(view.render());
        return false;
    },

    render: function () {
        $(this.el).html(this.template());
        $(".title-text").hide();
    }
});
