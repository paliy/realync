Realync.Views.Session.SignUp = Backbone.View.extend({

    template: JST['sessions/sign_up'],
    el: $("#login-bb"),

    initialize: function () {
        this_view = this;
    },

    events: {

        "click a[href='#!/sign_in']": "sign_in",
        "click label.client": 'trigger_client',
        "click  label.agent": 'trigger_agent'
    },
    trigger_agent:function(event){
        $('label.client, label.agent').removeClass('checked');
        $('label.agent').addClass('checked');
        $('ul[name="isRealtor"] input').prop('checked');
    },
    trigger_client:function(event){

        $('label.client, label.agent').removeClass('checked');
        $('label.client').addClass('checked');
    },
    check: function () {
        console.log('tada');
    },
    sign_in: function(e){

        window.location.hash = "!/sign_in";

        return false;
    },

    save_agent: function(data){
        var view = new Realync.Views.updateSubscription();
        view.render();
        window.location.hash = "!/updateSubscription";

    },

    render: function () {

        var user = new Realync.Models.User({
        });

        var form = new Backbone.Form({
            template: _.template($('#formTemplate').html()),
            model: user

        }).render();

        form.on('submit',function(event){
            $('.user-api-errors').html('') // clear errors

            var errors = form.commit();

            console.log(errors)

            if(! (errors.hasOwnProperty("zipcode") ||
                  errors.hasOwnProperty("email") ||
                  errors.hasOwnProperty("usename") ||
                  errors.hasOwnProperty("password"))){

                form.setValue({ mobile: false });
//                form.setValue({ realtor: true });
                var password = form.getValue('password');
                form.setValue({ confirmation: password });
                var data = form.getValue();
                console.log(data);
                var role = $("input:radio:checked").val();

                if(role=='false'){
                   user.sign_up(data, true);
                }else{
                    console.log('Agent');
                    user.sign_up(data, false);
                    console.log("9999999999999999999999999999999");
                    //this_view.save_agent(data);

                }
            }else{
                console.log('Bad');
            };
        })

       var template = $(this.el).html(this.template());
        $('#auth-page').html(form.el);
        $('ul[name="isRealtor"] li').first().find('input').attr('checked', 'checked');
        $('label.client').addClass('checked');
        $('ul[name="isRealtor"] li input').first().attr('id', 'client');

        $('ul[name="isRealtor"] li input').last().attr('id', 'agent');

    }

});