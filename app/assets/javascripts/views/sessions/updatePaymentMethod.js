Realync.Views.updatePaymentMethod = Backbone.View.extend({

    initialize: function(){
      this.profile_data = JSON.parse(localStorage.profile_info)
      pament_view = this;
    },

    template: JST['sessions/updatePaymentMethod'],
    el: "#login-bb",

    events: {
        "click #back_s":'back_subscription'
    },

    back_subscription: function(){

        var view = new Realync.Views.updateSubscription();
        view.render();
        window.location.hash = "!/updateSubscription"
        return false;
    },

    send_form_sabscription: function(){
       // $('form#braintree-payment-form input').css('border', '1px solid black');
//        var ajax_submit = function (e) {
             //var form = $('#braintree-payment-form');
//           e.preventDefault();
//           //$("#submit").attr("disabled", "disabled");
//           $.ajax({
//              url: '/pages/pay',
//              //url: 'http://stage.realync.com/user/startSubscription',
//              type: 'POST',
//              data: form.serialize(),
//              success: function(data){
//                console.log('Ok');
//                console.log(data);
//                form.parent().replaceWith(data);
//              },
//              error: function(data){
//                console.log('Error');
//                console.log(data);
//              }
//           });
//
//        }

    },

    getFormData: function($form){
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    },

    back_button: function(){
        $('.subscription_back').click(function(){
            Backbone.history.loadUrl();
            return false;
        });
    },

    render: function (settings) {
        console.log('777777777777777777777');
        console.log('777777777777777777777');
        console.log('777777777777777777777');
        console.log(this.model);
        var type = this.model.type;
        console.log(type);

        var card = new Realync.Models.Card({plan: type})


        if(settings == 'settings'){
            var form_s = new Backbone.Form({
                template: _.template($('#paymentSettingsTemplate').html()),
                model: card
            }).render();

            $('#account-content-bb').html(form_s.el);
            $('#account-content-bb').append('<a class="subscription_back" href="#">Back</a>')
            $('.back_from_form').hide();
            $('.settings-payment-method h3').html(type)
            this.back_button();
        }else{
            var form_s = new Backbone.Form({
                template: _.template($('#paymentTemplate').html()),
                model: card
            }).render();

            this.$el.html(this.template(this.template()));
            $('#payment-form').append(form_s.el);
        }



        $("#plan").val(type);


        $("#expirationmonth").parents().eq(2).css({'width': '210px', 'float' : 'left'});
        $("#expirationyear").parents().eq(2).css({'width': '80px', 'float' : 'right'});

        //$('form#braintree-payment-form input').css('border', '1px solid black');
        var ajax_submit = function () {
            var errors = form_s.commit() || {};
            console.log(errors);
            if(! Object.keys(errors).length > 0){

                var data = pament_view.getFormData($('#braintree-payment-form'));
                if(settings == 'settings'){
                    data.plan = pament_view.model.type;
                }
                console.log(data);
                var user = new Realync.Models.User({});
                if(pament_view.profile_data.planType !=  "Trial"){
                    user.changeSubscription(data);
                }else{
                    user.startSubscription(data);
                };

            }


//            try{
//                var errors = form_s.commit();
//            }catch(err){
//                console.log('111111111111111111111111111111')
//                console.log(errors)
//                console.log(err)
//                if(err.message == "Cannot read property 'set' of undefined"){
//                    console.log('111111111111122211111111111111111')
//                    if(! errors){
//                        console.log('1111111133331111111111111111111111')
//                        var user = new Realync.Models.User({});
//                        var data = pament_view.getFormData($('#braintree-payment-form'));
//                        console.log(data);
//                        console.log(pament_view.profile_data.planType)
//                        console.log(pament_view.profile_data.planType == 'Trial')
//                        if(pament_view.profile_data.planType !=  "Trial"){
//                            user.changeSubscription(data);
//                        }else{
//                            user.startSubscription(data);
//                        };
//                    };
//                }
//            }
//

            form = $('#braintree-payment-form');




        }
        console.log("ppppppppqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq!");
        var braintree =  Braintree.create("MIIBCgKCAQEAwhvIrhLu7EICw/7So5ZZhmcfe5jW4W9V55UMI3S3NHyv/UZgP/GCTdrZ56wad2kJY0eVLO5l6VbfZlCM+YqCxd6h7JbyBeTrBDiIrvgiVRl+08oy5nEnTcxYPYo08AMQGtY+rbtF7pDy3kVnmk4kkE6kxyMr7Xou5h7q3zAOtQ0y8T6g690DVopkFRGr3L10Wlx0L52YIkX1R1RHAAgrFk4L6CICp2vCf+2a+1/+iXObit1u2A27Ekrn8GExxnHyJ+s1d67OZd7m4s4ZAUQS/jXn/EketfKdi+FOTnhqAJLq2o5shSj4C61lrVOnk3/Gc4rT5eDVdB125asfaf66qwIDAQAB")

        braintree.onSubmitEncryptForm('braintree-payment-form', ajax_submit);



        return this;


    }
});