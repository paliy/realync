Realync.Views.Property_gallery_item = Backbone.View.extend({

    template: JST['properties/property_gallery_item'],
    tagName: "li",

    render: function () {
    	console.log('PHOTO ITEM');
        console.log(this.model);
        this.$el.html(this.template(this.model));
        return this;
    }
});