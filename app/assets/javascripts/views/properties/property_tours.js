Realync.Views.PropertyTours = Backbone.View.extend({

    template: JST['properties/property_tours'],
    tagName: "div class='view_property_attendees'",

    render: function () {
        console.log("Property Tours");
        console.log(this.model);
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});