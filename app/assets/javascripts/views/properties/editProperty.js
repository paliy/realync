Realync.Views.editProperty = Backbone.View.extend({

    template: JST['properties/editProperty'],
    el: "#page-content-bb",

	events: {
      'click .new_delete_video': 'delete_video'
    },

    delete_video: function () {
        console.log('delete video')
        console.log('delete video')
        console.log('delete video')
        var user_id = JSON.parse(localStorage.profile_info).user.id;
        var user = new Realync.Models.User;
        user.add_video_to_property('no_video', this.model.get('id'), user_id);
    },

    upload_cover_photo: function (id) {

        var property_id = this.property_id;
        var prop = new Realync.Models.Property;
        var  uploadButton = $('<button/>')
                .addClass('btn btn-primary cover-photo')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                        data = $this.data();
                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
            $('#fileupload').fileupload({
    //            url: "/pages/upload_image",
                url: localStorage.url + "/property/addPhoto/" + id,
                dataType: 'multipart/form-data',
                xhrFields: {
                    withCredentials: true
                },
                autoUpload: false,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5000000, // 5 MB

                success: function(data){
                    console.log('------------------data-----------------------');
                    console.log(data);
                },
                error: function(jqXHR, textStatus, errorMessage){

                    if(jqXHR.responseText && JSON.parse(jqXHR.responseText).status == 'SUCCESS'){

                        prop.set_thumbnail(JSON.parse(jqXHR.responseText).url, id);
                    } else {
                        alert("Can't connect to server");
                    }
                },
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                previewMaxWidth: 1065,
                previewMaxHeight: 400,
                previewOrientation: true,
                // previewCanvas: false
                previewCrop: true
            }).on('fileuploadadd', function (e, data) {

                $('#property_cover_image').html('');
                data.context = $('<div/>').appendTo('#property_cover_image');
                $.each(data.files, function (index, file) {
                    var node = $('<p class="cover_image_wrap"/>')
                        .append($('<span/>').text(file.name));
                    if (!index) {
                        node
                            .append('<br>')
                            .append(uploadButton.clone(true).data(data));
                    }
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {

                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            }).on('fileuploadprogressall', function (e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }).on('fileuploaddone', function (e, data) {

                $.each(data.result.files, function (index, file) {
                    if (file.url) {
                        var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                        $(data.context.children()[index])
                            .wrap(link);
                    } else if (file.error) {
                        var error = $('<span class="text-danger"/>').text(file.error);
                        $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                    }
                });
            }).on('fileuploadfail', function (e, data) {

                $.each(data.files, function (index, file) {
                    var error = $('<span class="text-danger"/>').text('File upload failed.');
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                });
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    upload_multiple_images: function( id ) {
        var  uploadButton = $('<button/>')
            .addClass('btn btn-primary edit_multiple_upload')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
        $('#multifileupload').fileupload({
            url: localStorage.url + '/property/addPhoto/' + id,
            dataType: 'multipart/form-data',
            async: true,
            cache: true,
            xhrFields: {
                withCredentials: true
            },
            processData: false,
            contentType: false,
            success: function(response) {
                // .. do something
                console.log('--success upload-------------------');
                console.log(response);
                $('#prop-multy-photo').css('z-index', 1);
                check_downloades();
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log(jqXHR.responseText);
                if(!jqXHR.responseText) {
                    $('.multi-image-wrap.progress').css("background-color", "#e10")
                                                   .text("can't connect to server");

                }
                console.log('--error upload------------------------');
                if(jqXHR.responseText && JSON.parse(jqXHR.responseText).status == 'SUCCESS'){
                    $('#multifiles .progress').css('display', 'none');
                    $('#multifiles .progress').removeClass('progress');        
                }
            },

            autoUpload: true,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 5000000, // 5 MB

            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 210,
            previewMaxHeight: 140,
            previewOrientation: true,
            previewCrop: false,
            progressall: function (e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#multifiles .progress').css(
                    'width',
                    progress + '%'
                );
              
            }
            // previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            console.log('add photo edit');

            data.context = $('<div class="large-3 columns end"/>').appendTo('#multifiles');
            $.each(data.files, function (index, file) {
                var node = $('<p class="multi-image-wrap"/>')
                    .append($('<span/>').text(file.name))
                    .append('<div class="delete-multy-item"><img class="delete-multy-item" src="assets/delete.png"></div>');
                if (!index) {
                    node
                        .append('<br>')
                        .append(uploadButton.clone(true).data(data))
                        .append('<div class="progress"><div class="progress-1 progress-bar-success">uploading...</div></div>');
                }
                node.appendTo(data.context);



            });
        }).on('fileuploadprocessalways', function (e, data) {

            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        }).on('fileuploadprogressall', function (e, data) {


        }).on('fileuploaddone', function (e, data) {
            

        }).on('fileuploadfail', function (e, data) {

        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },

    upload_video: function(id) {
        console.log('add upload video')
        var  uploadButton = $('<span/>')
            .addClass('video_upload')
            .prop('disabled', true)


        $('#button_upload').fileupload({
            forceIframeTransport: true,
            dataType: 'multipart/form-data',
            async: true,
            cache: true,
            xhrFields: {
                withCredentials: true
            },
            processData: false,
            contentType: false,
            success: function(response) {
                // .. do something
                console.log('--success upload-------------------');
                console.log(response);
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log('--error upload------------------------');
                console.log(jqXHR);
            },

            autoUpload: false,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,

            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 210,
            previewMaxHeight: 120,
            previewOrientation: true
            // previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            console.log('addPhoto');
            console.log(data);
            $('#video').html('');
            data.context = $('<div class="large-3 columns end"/>').appendTo('#video');
            $.each(data.files, function (index, file) {
                var node = $('<p class="multi-image-wrap"/>')
//                    .append($('<span/>').text(file.name));
                if (!index) {
                    node
                        .append('<br>')
                        .append(uploadButton.clone(true).data(data));
                }
                node.appendTo(data.context);
                $('.new_delete_video').css('display', 'block');
                $('#video').addClass('columns')
                if( $('.video_upload').data()){
                    var arr = $('.video_upload').data().files[0].name.split('.')
                    arr.splice(arr.length-1, 1);
                    var filename = ''
                    for(i in arr){
                        filename += arr[i]
                    }
                    var user = new Realync.Models.User;
                    var user_id = JSON.parse(localStorage.profile_info).user.id;
                    user_model.upload_to_s3(filename, id, user_id);
                    user.add_video_to_property(filename, id, user_id);

                };
            });
        }).on('fileuploadprocessalways', function (e, data) {

            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        })
    },

    add_video_preview: function (url) {
        var view = new Realync.Views.Video_preview({model: {url: url}})
        $('#video').prepend(view.render().el);
        $('.new_delete_video').css('display', 'block');
    },

    show_photos: function (photos, id) {

        for(i in photos){
            console.log(photos[i]);
            var view = new Realync.Views.Edit_photo_item({model: {url: photos[i], id: id}});
            $('#multifiles').append(view.render().el);
        }

    },


    render: function () {

        $(this.el).html(this.template(this.model.toJSON()));
        // this.property_id = property.id;
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        this.show_photos();
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.properties').children().addClass('active_link_dash');

        console.log(this.model);

    }

});