Realync.Views.newProperty = Backbone.View.extend({

    template: JST['properties/newProperty'],
    el: "#page-content-bb",

    events: {
        "click label.cover_image_upload img" : "upload_cover_photo"
    },

    upload_cover_photo: function (event) {
    	var property = new Realync.Models.Property;
        var  uploadButton = $('<button/>')
                .addClass('btn btn-primary cover-photo')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                        data = $this.data();
                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function () {
//                        $this.remove();
                    });
                });
            $('#fileupload').fileupload({
    //            url: "/pages/upload_image",
                url: localStorage.url + "/property/addPhoto/" + localStorage.new_prop_id,
                dataType: 'multipart/form-data',
                xhrFields: {
                    withCredentials: true
                },
                autoUpload: false,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5000000, // 5 MB
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                success: function(data){
                    console.log('------------------data-----------------------');
                    console.log(data);
                    $("<style>#prop-multy-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                    check_downloades();
                },
                error: function(jqXHR, textStatus, errorMessage){
                	console.log(errorMessage);
                    if(jqXHR.responseText && JSON.parse(jqXHR.responseText).status == 'SUCCESS'){
                        set_process_text('cover photo load success', 'cover');

                    	property.set_thumbnail(JSON.parse(jqXHR.responseText).url, localStorage.new_prop_id);

                    }else{
                        set_process_text('cover photo load fail', 'cover');
                        $("<style>#prop-cover-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                        $('#prop-cover-photo').css('z-index','1');

                    }
                    check_downloades();
                },
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                previewMaxWidth: 1065,
                previewMaxHeight: 400,
                previewOrientation: true,
                previewCrop: true,
                progressall: function (e, data)
                {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    set_process_progres('cover', progress);
                    set_process_text('load cover photo', 'cover');
                }
            }).on('fileuploadadd', function (e, data) {
            	// console.log(data);
                $('#property_cover_image').html('');
                data.context = $('<div/>').appendTo('#property_cover_image');
                $.each(data.files, function (index, file) {
                    var node = $('<p class="cover_image_wrap"/>')
                        .append($('<span/>').text(file.name));
                    if (!index) {
                        node
                            .append('<br>')
                            .append(uploadButton.clone(true).data(data));
                    }
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {

                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            }).on('fileuploadprogressall', function (e, data) {
            }).on('fileuploaddone', function (e, data) {
            }).on('fileuploadfail', function (e, data) {
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },

    upload_multiple_images: function() {
    	console.log("#multifiles.files");
        var  uploadButton = $('<button/>')
                .addClass('btn btn-primary multiple_upload')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                        data = $this.data();
                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
	    $('#multifileupload').fileupload({
//            url: "/pages/upload_image",
//            url: "http://stage.realync.com/user/addPhoto",
            url: localStorage.url + '/property/addPhoto/' + localStorage.new_prop_id,
            dataType: 'multipart/form-data',
            async: true,
            cache: true,
            xhrFields: {
                withCredentials: true
            },
            processData: false,
            contentType: false,
            success: function(response) {
                // .. do something
                console.log('--success upload-------------------');
                console.log(response);
                $("<style>#prop-multy-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                $('#prop-multy-photo').css('z-index', 1);
                Backbone.history.loadUrl();
                check_downloades();
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log('--error upload------------------------');
                $("<style>#prop-multy-photo { z-index: 1; }</style>").appendTo(document.documentElement);
//                $('#prop-multy-photo').css('z-index', 1);
                Backbone.history.loadUrl();
                check_downloades();
			},

	        autoUpload: false,
	        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
	        maxFileSize: 5000000, // 5 MB
	        // Enable image resizing, except for Android and Opera,
	        // which actually support image resizing, but fail to
	        // send Blob objects via XHR requests:
	        disableImageResize: /Android(?!.*Chrome)|Opera/
	            .test(window.navigator.userAgent),
	        previewMaxWidth: 210,
	        previewMaxHeight: 120,
	        previewOrientation: true,
            progressall: function (e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                set_process_progres('multy',progress);
                set_process_text('load photos', 'multy');
            }
	        // previewCrop: true
        }).on('fileuploadadd', function (e, data) {
        	console.log('addPhoto');
        	console.log(data);
            data.context = $('<div class="large-3 columns end"/>').prependTo('#multifiles');
            $.each(data.files, function (index, file) {
                var node = $('<p class="multi-image-wrap"/>')
                    .append($('<span/>').text(file.name))
                    .append('<div class="delete-multy-item"><img class="delete-multy-item" src="assets/delete.png"></div>');
                if (!index) {
                    node
                        .append('<br>')
                        .append(uploadButton.clone(true).data(data));
                }
                node.appendTo(data.context);



            });
        }).on('fileuploadprocessalways', function (e, data) {

            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        }).on('fileuploadprogressall', function (e, data) {


        }).on('fileuploaddone', function (e, data) {

            $.each(data.result.files, function (index, file) {
                if (file.url) {
                    var link = $('<a>')
                        .attr('target', '_blank')
                        .prop('href', file.url);
                    $(data.context.children()[index])
                        .wrap(link);
                } else if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                }
        	});
        }).on('fileuploadfail', function (e, data) {

            $.each(data.files, function (index, file) {
                var error = $('<span class="text-danger"/>').text('File upload failed.');
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
        	});
        }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },

    upload_video: function() {
        var  uploadButton = $('<span/>')
            .addClass('video_upload')
            .prop('disabled', true)


        $('#file_upload_amazon').fileupload({
//            url: "/pages/upload_image",
//            url: "http://stage.realync.com/user/addPhoto",
            forceIframeTransport: true,
            dataType: 'multipart/form-data',
            async: true,
            cache: true,
            xhrFields: {
                withCredentials: true
            },
            processData: false,
            contentType: false,
            success: function(response) {
                // .. do something
                console.log('--success upload-------------------');
                console.log(response);
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log('--error upload------------------------');
                console.log(jqXHR);
            },

            autoUpload: false,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,

            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 210,
            previewMaxHeight: 120,
            previewOrientation: true
            // previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            console.log('addPhoto');
            console.log(data);
            $('#video').html('');
            data.context = $('<div class="large-3 columns end"/>').appendTo('#video');
            $.each(data.files, function (index, file) {
                var node = $('<p class="multi-image-wrap"/>')
//                    .append($('<span/>').text(file.name));
                if (!index) {
                    node
                        .append('<br>')
                        .append(uploadButton.clone(true).data(data));
                }
                node.appendTo(data.context);
                $('.new_delete_video').css('display', 'block');
                $('#video').addClass('columns')
            });
        }).on('fileuploadprocessalways', function (e, data) {

            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            if (file.error) {
                node
                    .append('<br>')
                    .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
            }
        })
     },


render: function () {
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.properties').children().addClass('active_link_dash');
    }
});

$(document).on('click','.delete-multy-item',function(){
    if($(this).hasClass('exist')) {
        var prop = new Realync.Models.Property;
        var id = $(this).attr('data-id');
        var src = $(this).attr('data-src');
        prop.delete_photo(id, src, false);
        delete $(this).parents().eq(2);
        delete $(this).parents().eq(2).remove();
    } else {
        delete $(this).parents().eq(2);
        delete $(this).parents().eq(2).remove();
    }
});

$(document).on('click','.new_delete_video',function(){
    delete $('#video').children();
    $('#video').children().remove();
    $(this).css('display', 'none');
    $('#video').removeClass('columns');

});


function createCORSRequest(method, url)
{
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr)
    {
        xhr.open(method, url, true);
    }
    else if (typeof XDomainRequest != "undefined")
    {
        xhr = new XDomainRequest();
        xhr.open(method, url);
    }
    else
    {
        xhr = null;
    }
    return xhr;
};


function uploadToS3(file, url, content_type)
{

    var xhr = createCORSRequest('PUT', url);
    if (!xhr)
    {
//        setProgress(0, 'CORS not supported');
    }
    else
    {
        xhr.onload = function()
        {
            if(xhr.status == 200){
//                setProgress(100, 'Upload completed.');
                $("<style>#prop-video-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                $('#prop-video-photo').css('z-index', 1);
                check_downloades();
            }
            else{
//                setProgress(0, 'Upload error: ' + xhr.status);
                $("<style>#prop-video-photo { z-index: 1; }</style>").appendTo(document.documentElement);
                $('#prop-video-photo').css('z-index', 1);
                check_downloades();
            }
        };
        xhr.onerror = function(){
//            setProgress(0, 'XHR error.');
        };
        xhr.upload.onprogress = function(e){
            if (e.lengthComputable)
            {
                var percentLoaded = Math.round((e.loaded / e.total) * 100);
//                setProgress(percentLoaded, percentLoaded == 100 ? 'Finalizing.' : 'Uploading.');
                console.log(percentLoaded);
                set_process_progres('video',percentLoaded);
                set_process_text('load video', 'video');
            }
        };

        xhr.setRequestHeader('Content-Type', content_type);
        //xhr.setRequestHeader('x-amz-acl', 'public-read');
        xhr.send(file);
    }
}