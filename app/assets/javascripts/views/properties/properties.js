Realync.Views.Properties = Backbone.View.extend({

    template: JST['properties/properties'],
    el: "#page-content-bb",

    render: function () {
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".link2 a").css({"color":"#1ca5c9"});
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    }
});