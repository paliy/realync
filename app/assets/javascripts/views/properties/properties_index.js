Realync.Views.Properties_index = Backbone.View.extend({

    template: JST['properties/index'],
    el: "#page-content-bb",

    events: {
        "click #delete" : "delete_property"
    },

    initialize: function() {
        // alert('init');
    },

    delete_property: function () {
        var prop = new Realync.Models.Property;
        var arr = window.location.hash.split('/');
        var id = arr[arr.length - 1];
        prop.delete(id);
    },

    render: function () {
		$(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.properties').children().addClass('active_link_dash');
    }
});

