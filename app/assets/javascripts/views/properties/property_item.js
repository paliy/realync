Realync.Views.Property_list_view = Backbone.View.extend({

    template: JST['properties/property_item'],
    tagName: 'div',

    events: {
        "click .property_item" : "schedule_property",
        "click .delete_property" : "delete_property"
    },

    schedule_property: function(){
        localStorage.property_schedule_id = this.model.get("id");
        window.location = "#!/createTour";
        return false;
    },

    delete_property: function () {
        var id = this.model.get('id');
        this.model.delete(id, 'index');
        return false;
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));

        return this;
    }
});
