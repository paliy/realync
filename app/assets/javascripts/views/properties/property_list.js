Realync.Views.Property_list = Backbone.View.extend({

    template: JST['properties/property_list'],
    el: "#property_list",

    addPropertyList: function(){
        var ul = $('.view-property');
        ul.html('');
        Realync.properties_collection.each(this.addPropertyItem, this);
    },
    addPropertyItem: function(property){
        var ul = $('.view-property');
        var view = new Realync.Views.Property_list_view({model: property});
        ul.append(view.render().el);
        if ((JSON.parse(localStorage.profile_info).user.id) != property.attributes.realtor) {
            $(".properties-block").remove();
        }
    },

    render: function () {
        console.log("prop list");
        $(this.el).html(this.template());
        this.addPropertyList()
    }
});