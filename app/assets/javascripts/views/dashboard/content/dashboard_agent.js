Realync.Views.agentDashboard = Backbone.View.extend({

    template: JST['dashboard/content/dashboard_agent'],
    el: "#page-content-bb",

    addUpcomingTour: function(tour){
        console.log("one prop");

        var view = new Realync.Views.Dashboard.Tours_item({model: tour});

       $('.dashboard-tours').append(view.render().el);
        console.log('----------------------2222222222222-------------------------')
        console.log(tour)
        $("#" + tour.get("id")).html("");
        var attendees = tour.get("accessList");

        for (a in attendees){
            if(a < 4){
                var view = new Realync.Views.Attendies({model: attendees[a]});
                $("#" + tour.get("id")).append(view.render().el);
            }else{
                var view = new Realync.Views.Many_attendies({ model: { length: attendees.length - 4 } });
                $("#" + tour.get("id")).append(view.render().el);
                return ;
            }
        }
    },

    addAllUpcomingTours: function(){
        if (Realync.upcoming_collection.models.length > 0) { 
            $(".dashboard-tours").html("");
            Realync.upcoming_collection.each(this.addUpcomingTour, this);
                $('[href="#!/tour_list"]').show();
                $("#upcoming-container-client").css("display", "block");
        }
        else {
            $('[href="#!/tour_list"]').hide();
        }
    },

    view_message: function(){
        var mess_id = $(this).find('.item').attr('data-item');
        window.location = "#!/messages/" + mess_id;
    },

    render: function () {
        $(this.el).html(this.template());
        $("#content").removeClass('large-12').addClass('large-8');
        $(".large-4.columns#rightbar").fadeIn();
        this.addAllUpcomingTours();
//        this.addMessageList();
        if(!$.isEmptyObject(JSON.parse(localStorage.profile_info).myAgent)) {
            this.addAgentInfo();
        }
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.dashboard').children().addClass('active_link_dash');
        $(document).on('click', '.messages_list_in_dashboard', this.view_message);

        if(JSON.parse(localStorage.profile_info).user.realtor){
            var agent_profile = JSON.parse(localStorage.profile_info)
            var remaining_minutes = agent_profile.planSeconds / 60
            var usedSeconds = agent_profile.usedSeconds / 60
            $("#planSeconds").prepend('<div class="minutes-overall">' 
                                    + '<span class="used-seconds">' 
                                    + usedSeconds.toFixed(1) 
                                    + '</span>' + " / " + remaining_minutes 
                                    + " Minutes Remaining" 
                                    + '</div>')
            var progress = usedSeconds*100/remaining_minutes;
            $('.remain-minutes').width(progress + '%');
        }
    }
});