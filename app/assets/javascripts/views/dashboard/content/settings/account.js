Realync.Views.Account = Backbone.View.extend({

    template: JST['dashboard/content/settings/account'],
    el: "#account-content-bb",

    events: {

        "click #delete_account_button" : 'delete_user_account'
    },

    delete_user_account: function(){
        var current_user = new Realync.Models.User(JSON.parse(localStorage.user));

        current_user.delete_account();
        delete localStorage.user;
        window.location = "landing#!/";
        localStorage.clear();
    },

    render: function () {
        var current_user = new Realync.Models.User(JSON.parse(localStorage.user));
        var form = new Backbone.Form({

            model: current_user,
            template: _.template($('#accountTemplate').html())
        }).render();

        form.on("submit", function(){
            var data = form.getValue();
            errors = form.commit();

            if($('#check_with_email').prop('checked')){
                data.resetEmail = 'true';

            }else{
                data.resetEmail = 'false';
            }

            if(!errors.hasOwnProperty("new_password") && !errors.hasOwnProperty("confirmation")
               && (!errors.hasOwnProperty("old_password") ||  data.resetEmail == 'true')){
                current_user.change_password(data);
            }

        })

        $("#content").removeClass('large-8').addClass('large-12');
        this.$el.html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $('#account-form').html(form.el);
        $("#change-pass-block").hide();
        $("a.account_link").css({
            'color' : '#1ca5c9',
            'text-decoration' : 'underline'
            });
        $("a.sub_link").css('color', '#a4a4a4');
        $("a.prof_link").css('color', '#a4a4a4');
        $("a.shar_link").css('color', '#a4a4a4');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.settings').children().addClass('active_link_dash');

    }
});

$(document).on('click', '#check_with_email', function(){
    if($(this).prop('checked')){
        $("#current_password").hide();
    }else{
        $("#current_password").show();
    }
})