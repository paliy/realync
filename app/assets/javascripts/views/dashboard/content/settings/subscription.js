Realync.Views.Subscription = Backbone.View.extend({
    initialize: function(){
        subscription_page = this;
    },


    template: JST['dashboard/content/settings/subscription'],
    el: "#account-content-bb",

    events: {
        "click #payment_button" : 'change_payment_method'

    },


    change_payment_method: function() {

        this.payment_form();
        console.log('change payment');
        $("#change_payment_method").hide();
        $('.delete_card').hide();
        $("#block-payment").show();
        $('label[for="last_name"], label[for="postal_code"] ').parent().remove()
    },


    add_cards: function(){
        var card = new Realync.Models.Card
        card.getCards();
    },



    show_payment_form: function(){
        var user = new Realync.Models.User;
        $('#basic').click(function(){
            var data = { plan: 'Trial', cardholderName: '', month: '', year: '', number: '', cvv: '' }
            user.startSubscription(data);
            window.location = "/dashboard#!/"
            return false;
        });
        $('#plus').click(function(){
            var view = new Realync.Views.updatePaymentMethod({model: {type: "Plus"}});
            view.render('settings');
            return false;
        });
        $('#premium').click(function(){
            var view = new Realync.Views.updatePaymentMethod({model: {type: "Premium"}});
            view.render('settings');
            return false;
        });
    },
    getFormData: function($form){
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    },

    payment_form: function(){
        var card = new Realync.Models.Card
        var payment_form = new Backbone.Form({
            template: _.template($('#paymentSettingsTemplate').html()),
            model: card
        }).render();

        $('#payment-settings-form').html(payment_form.el);
        $('[name="last_name"], [name="postal_code"] ').parent().parent().parent().remove();
        delete $('[name="last_name"], [name="postal_code"] ').parent().parent().parent()

        $('.back_from_form').click(function(){
            $("#change_payment_method").show();
            $("#block-payment").hide();
            return false;
        });

        var braintree =  Braintree.create("MIIBCgKCAQEAwhvIrhLu7EICw/7So5ZZhmcfe5jW4W9V55UMI3S3NHyv/UZgP/GCTdrZ56wad2kJY0eVLO5l6VbfZlCM+YqCxd6h7JbyBeTrBDiIrvgiVRl+08oy5nEnTcxYPYo08AMQGtY+rbtF7pDy3kVnmk4kkE6kxyMr7Xou5h7q3zAOtQ0y8T6g690DVopkFRGr3L10Wlx0L52YIkX1R1RHAAgrFk4L6CICp2vCf+2a+1/+iXObit1u2A27Ekrn8GExxnHyJ+s1d67OZd7m4s4ZAUQS/jXn/EketfKdi+FOTnhqAJLq2o5shSj4C61lrVOnk3/Gc4rT5eDVdB125asfaf66qwIDAQAB")
        braintree.onSubmitEncryptForm('braintree-payment-form', function(){
            console.log('SUBMIT')
            var errors = payment_form.commit()
            delete errors.last_name
            delete errors.postal_code
            if(!Object.keys(errors).length > 0){

                var data = subscription_page.getFormData('#braintree-payment-form');
                if(!data.plan){
                    data.plan = JSON.parse(localStorage.profile_info).planType
//                    data.plan = "Plus"
                    data.last_name = JSON.parse(localStorage.profile_info).lastName
                    data.postal_code = JSON.parse(localStorage.profile_info).zipcode
                }
                console.log(data)
                console.log(data)
                var user = new Realync.Models.User;
                user.startSubscription(data);


            }

            console.log(data)
        });


        payment_form.on('submit', function(){


            var data = payment_form.getValue()
            console.log(data)
            console.log(data)
            console.log(data.plan)

        });
    },

    getFormData: function(form){
        var unindexed_array = $(form).serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    },


    style_links: function(){
        $('#block-payment').hide();
        $("#expirationmonth").parents().eq(2).css({'width': '200px', 'float' : 'left'});
        $("#expirationyear").parents().eq(2).css({'width': '190px', 'float' : 'right'});
        $("#card_number").parents().eq(2).css({'width': '230px', 'float' : 'left'});
        $("#security_code").parents().eq(2).css({'width': '150px', 'float' : 'right'});

        $("a.acount_link").css('color', '#a4a4a4');
        $("a.sub_link").css({
            'color' : '#1ca5c9',
            'text-decoration' : 'underline'
        });
        $("a.prof_link").css('color', '#a4a4a4');
        $("a.shar_link").css('color', '#a4a4a4');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.settings').children().addClass('active_link_dash');
    },

    render_template: function(){
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $("#content").removeClass('large-8').addClass('large-12');
    },

    render: function () {
        $(this.el).html(this.template({role: this.model.role}));
        this.payment_form();
        this.render_template();
        this.show_payment_form();
        this.style_links();
        this.add_cards();

    }
});
