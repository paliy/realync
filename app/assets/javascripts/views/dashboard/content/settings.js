Realync.Views.Settings = Backbone.View.extend({

    template: JST['dashboard/content/settings'],
    el: "#page-content-bb",


    render_template: function(){
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    },

    render: function(){

        $(this.el).html(this.template({role: this.model.role}));
        console.log("settings");
        this.render_template();
    }

});