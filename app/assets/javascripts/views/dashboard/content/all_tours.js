Realync.Views.All_tours = Backbone.View.extend({

    template: JST['dashboard/content/all_tours'],
    events: {
        "click .join_tour" : "join_tour"
    },

    join_tour: function(){
        window.location = "#!/live_tour/" + this.model.get("id");
        //this.model.createTourOpentok(this.model.get("videoSession"), "agent");
        return false;

    },
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});