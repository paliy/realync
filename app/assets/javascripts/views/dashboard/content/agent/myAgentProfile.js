Realync.Views.MyAgentProfile = Backbone.View.extend({

    template: JST['dashboard/content/agent/myAgentProfile'],
    el: "#page-content-bb",

    events: {
        "click .leaveAgent" : "leaveMyAgent",
        "click #mess_me" : "message_me"
    },

    message_me: function(){
        localStorage.send_users_arr =  JSON.stringify([this.model.get("user").name]);
        window.location = "#!/messages";
        return false;
    },

    leaveMyAgent: function() {
        var user = new Realync.Models.User;
        console.log(user);
        user.leaveAgent();
        return false;
    },

    render: function (model) {
        $(this.el).html(this.template(model.toJSON()));
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    }

});