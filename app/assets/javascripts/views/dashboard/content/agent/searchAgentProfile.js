Realync.Views.SearchAgentProfile = Backbone.View.extend({

    template: JST['dashboard/content/agent/searchAgentProfile'],
    el: "#page-content-bb",

    events: {
        "click .request_connection" : "request_connection",
        "click #mess_me_agent" : "message_to_agent"
    },

    message_to_agent: function(){
        localStorage.send_users_arr =  JSON.stringify([this.model.get("user").name]);
        window.location = "#!/messages";
        return false;
    },

    request_connection: function() {
        var user = new Realync.Models.User;
        var params = this.model.attributes;

            var data = {agent: '{                                \
            "id": "'+ params.user.id + '",                        \
            "name":"' + params.user.name + '",                    \
            "profilePicUrl": "'+  params.user.mobilePicUrl + '",  \
            "email":"' + params.user.email + '"                   \
        }'};

        user.setAgent(data);
        return false;
    },

    render: function (model) {
        this.model = model;
        $(this.el).html(this.template(model.toJSON()));
        if (JSON.parse(localStorage.profile_info).user.realtor) {
            $(".request_connection").css({"display":"none"});        }
        if (!JSON.parse(localStorage.profile_info).user.realtor) {
            if (!$.isEmptyObject(JSON.parse(localStorage.profile_info).myAgent)) {
                $(".request_connection").css({"display":"none"});
            }
        }
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');

    }

});