Realync.Views.Agent_info = Backbone.View.extend({

    template: JST['dashboard/content/agent_info'],
    tagName: 'div',

    events: {
      "click #message_to_agent" : "message_to_my_agent"
    },

    message_to_my_agent: function(){
        localStorage.send_users_arr =  JSON.stringify([this.model.get("name")]);
        window.location = "#!/messages";
        return false;
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        console.log(this.model);
        return this;
    }

});