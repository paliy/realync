Realync.Views.Agents = Backbone.View.extend({

    template: JST['dashboard/content/agents'],
    el: "#page-content-bb",

    addAgent: function(agent){
        console.log("one Agent");

        var view = new Realync.Views.All_agents({model: agent});

        $('.dashboard-all-agents').append(view.render().el);
    },

    addAllAgents: function(){
        console.log("all Agent");
        console.log(Realync.agent_collection);
        $(".dashboard-all-agents").html("");
        Realync.agent_collection.each(this.addAgent, this);
    },


    render: function(){
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        console.log("Render agent");
        this.addAllAgents();
    }

});