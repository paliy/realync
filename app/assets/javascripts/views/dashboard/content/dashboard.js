Realync.Views.Dashboard = Backbone.View.extend({

    template: JST['dashboard/content/dashboard'],
    el: "#page-content-bb",



    addUpcomingTour: function(tour){
        var view = new Realync.Views.Dashboard.Tours_item({model: tour});

       $('.dashboard-tours').append(view.render().el);
    },

    addAllUpcomingTours: function(){
        $(".dashboard-tours").html("");


        Realync.upcoming_collection.each(this.addUpcomingTour, this);
    },
    addMessageItem: function(message){

        var ul = $('.message_preview_list');
        var view = new Realync.Views.MessageList({model: message});
        ul.append(view.render().el);
    },
    addMessageList: function(){

        var ul = $('.message_preview_list');
        ul.html('');
        Realync.realyncTestList.each(this.addMessageItem, this);
    },

    addAgentInfo: function(){
           var agent = new Realync.Models.MyAgent(JSON.parse(localStorage.profile_info).myAgent);
        var view = new Realync.Views.Agent_info({model: agent});
        var ul = $('#agent-info');
        ul.html(view.render().el);

    },

    render: function () {
        $(this.el).html(this.template());
        $("#content").removeClass('large-12').addClass('large-8');
        $(".large-4.columns#rightbar").fadeIn();
        if(localStorage.profile_info && !$.isEmptyObject(JSON.parse(localStorage.profile_info).myAgent)) {
            this.addAgentInfo();
        }
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.dashboard').children().addClass('active_link_dash');


    }
});