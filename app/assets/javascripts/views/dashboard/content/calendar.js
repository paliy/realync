Realync.Views.Calendar = Backbone.View.extend({
    initialize: function(){
        calendar_model = this;
    },

    template: JST['dashboard/content/calendar'],
    el: "#page-content-bb",

    render: function(){

        this.$el.html(this.template(this.template()));
        console.log("CALENDAR");
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $("a.calendar-link").css('color', '#1ca5c9');
        $("a.list-link").css('color', '#a4a4a4');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.tours').children().addClass('active_link_dash');


        $(document).ready(function() {

	        $('span.has-tip').mouseleave(function() {return false;});
		    $('span.has-tip').mouseenter(function() {return false;});
	        $('span.has-tip').click(function(e){
	        	if($('span.tooltip').is(':visible')) {
	        		$('span.tooltip').hide();
	        	} else {
	        		$('span.tooltip').show();
	        		// var x = e.pageX - e.target.offsetLeft;
	        		// $('span.tooltip').css('left', x + 'px');\
	        		var pos = $(this).offset();
	        		var size = $(this).position();
	        		var size_ttp = $('span.tooltip').position();
	        		console.log(pos);
	        		console.log(size);
	        		console.log(size_ttp);
	        		if(pos.left > $(window).width()/2){
						$('span.tooltip').css('left', (pos.left - ($('span.tooltip').width() + size.left + 15)) + 'px');
		        		$('span.tooltip').css('top', (pos.top - $("span.tooltip").height()/2) + 'px');
		        		$('span.tooltip').removeClass('tip-right').addClass('tip-left');
					} else {
		        		$('span.tooltip').css('left', (pos.left + $(this).width() + 7) + 'px');
		        		$('span.tooltip').css('top', (pos.top - $("span.tooltip").height()/2) + 'px');
		        		$('span.tooltip').removeClass('tip-left').addClass('tip-right');
	        		}



	        	}
	        });
    	    var date = new Date();
		    var d = date.getDate();
		    var m = date.getMonth();
		    var y = date.getFullYear();

		    // page is now ready, initialize the calendar...
		    var calendar = $('#calendar');


		    calendar.fullCalendar({

				header:	{
					left: '',
					center: 'prev title next',
					right: ''
				},

				columnFormat: {
				   month: 'dddd'
				},

				weekMode: 'liquid',

                eventRender: function(event, element, calEvent) {
                	element.find(".fc-event-title").html('<div class="start_time">' + event.start_time + '</div>' + ' ' + event.title);
			    	element.find(".fc-event-title").before($("<span class=\"fc-event-icons\" id=\""+event.model.id+"\"></span>").html("<img src=\""+ event.property_url +"\" />"));
			    	$('.fc-event').off('click');
			    	$('.fc-event').off('click');
//			        $('.fc-event, #tour_view').click({param: event}, call_function) ;
                    if(event.type == 'past'){
                        $('#'+ event.model.id).parent().css('opacity', 0.5) ;
                    }

                    $('#'+ event.model.id).parent().click({param: event}, call_function) ;
                    function call_function(event) {
                        console.log(event)
                        console.log(event.param)
			        	$('.fc-event-title').removeClass('active_event');
                        var size_field = $(".fc-day-content").width();
                        var pos = $(this).offset();
                        var size = $(this).position();
                        var size_ttp = $('#tour_view').position();
                        if(pos.left > $(window).width()/2){
                           $('#tour_view').css('left', (pos.left - size_field - 335) + 'px');
                           $('#tour_view').css('top', (pos.top - 400/2 - 70) + 'px');
//                            $('#tour_view').removeClass('tip-right').addClass('tip-left');
                        } else {
                        	$('#tour_view').css('left', (pos.left - size_field + 56) + 'px');
                           	$('#tour_view').css('top', (pos.top - 400/2 - 70) + 'px');
                            // $('#tour_view').removeClass('tip-left').addClass('tip-right');
                        }

                        var view = new Realync.Views.TourItemList({model: event.data.param.model });
                        $("#tour_view").html(view.render().el);
                        $('#tour_view').show();
                        $(this).find('.fc-event-title').addClass('active_event');
                        event.stopPropagation();
                        $('.tour-container').click(function(e){
                        	e.stopPropagation();
                        });
			    	};
			    }
		    })
		});
		$("html").click(function() {
			$('.fc-event-title').removeClass('active_event');
			$("#tour_view").hide();
		});	
		 $(document).foundation();
    }

});