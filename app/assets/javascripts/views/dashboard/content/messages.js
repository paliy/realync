Realync.Views.Messages = Backbone.View.extend({

    template: JST['dashboard/content/messages'],
    el: "#page-content-bb",

    render: function(){
        $(this.el).html(this.template());
    }

});