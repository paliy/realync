Realync.Views.MyAgent = Backbone.View.extend({

    template: JST['dashboard/content/dashboard/client/my_agent'],
    el: "#page-content-bb",

    render: function () {
        $(this.el).html(this.template());
    }
});