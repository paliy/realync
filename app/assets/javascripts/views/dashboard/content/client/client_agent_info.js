Realync.Views.Client_agent_info = Backbone.View.extend({

    template: JST['dashboard/content/client/client_agent_info'],
    tagName: 'div',

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }

});