Realync.Views.All_agents = Backbone.View.extend({

    template: JST['dashboard/content/all_agents'],
    events: {
        "click .button.expand.view_agent" : "view_agent_profile"

    },
    view_agent_profile: function () {
        var view = new Realync.Views.AgentProfile;
        view.render(this.model);
        return false;
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }

});