Realync.Views.Profile = Backbone.View.extend({

    template: JST['dashboard/content/profile'],
    el: "#page-content-bb",
    events: {
        "click #switch_content .switch" : "trigger_active_search",
        "click label.photo-width img" : "upload_photo"
//        "click #submit_form.button": "update_logo"
    },


    trigger_active_search: function (event) {
        if($('#switch_content .switch input').first().prop('checked')) {
            $('#switch_content .switch').css('border', '2px solid #1dcb9a');
        } else {
            $('#switch_content .switch').css('border', '2px solid #38414e');
        }
    },

    upload_photo: function (event) {
        var  uploadButton = $('<button/>')
                .addClass('btn btn-primary')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function () {
                    var $this = $(this),
                        data = $this.data();
                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function () {
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function () {
                        $this.remove();
                    });
                });
            $('#fileupload').fileupload({
                url: "http://stage.realync.com/user/addPhoto",
                dataType: 'multipart/form-data',
                xhrFields: {
                    withCredentials: true
                },
                autoUpload: false,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5000000, // 5 MB

                success: function(data){
                    console.log('------------------data-----------------------');
                    console.log(data);
                },
                error: function(jqXHR, textStatus, errorMessage){

                    if(JSON.parse(jqXHR.responseText).status == 'SUCCESS'){
                        localStorage.profile_image = JSON.parse(jqXHR.responseText).url;
                        $('#profile img').attr('src', JSON.parse(jqXHR.responseText).url) ;
                    }
                },
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                previewMaxWidth: 120,
                previewMaxHeight: 120,
                previewOrientation: true,
                previewCrop: true
            }).on('fileuploadadd', function (e, data) {

                $('#files').html('');
                data.context = $('<div/>').appendTo('#files');
                $.each(data.files, function (index, file) {
                    var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                    if (!index) {
                        node
                            .append('<br>')
                            .append(uploadButton.clone(true).data(data));
                    }
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {

                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            }).on('fileuploadprogressall', function (e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }).on('fileuploaddone', function (e, data) {

                $.each(data.result.files, function (index, file) {
                    if (file.url) {
                        var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                        $(data.context.children()[index])
                            .wrap(link);
                    } else if (file.error) {
                        var error = $('<span class="text-danger"/>').text(file.error);
                        $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                    }
                });
            }).on('fileuploadfail', function (e, data) {

                $.each(data.files, function (index, file) {
                    var error = $('<span class="text-danger"/>').text('File upload failed.');
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                });
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    render_topbar: function(){
        if($('#topbar-bb img').length === 0){
          (new Realync.Views.Topbar).render();
        }
    },

    render: function(model){
        if(localStorage.profile_image) {
            model.set("mobilePicUrl", localStorage.profile_image);
        } else {
            model.set("mobilePicUrl", model.attributes.user.mobilePicUrl);
        }
        $(this.el).html(this.template(model.toJSON()));
        (new Realync.Views.Sidebar).render();
        this.render_topbar();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    }

});