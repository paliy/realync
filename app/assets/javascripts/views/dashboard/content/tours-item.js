Realync.Views.Dashboard.Tours_item = Backbone.View.extend({

    template: JST['dashboard/content/tours_list'],
    tagName: 'li',

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }

});