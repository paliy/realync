/**
 * Created by vitaliyjorzh on 16/1/14.
 */
Realync.Views.Rightbar = Backbone.View.extend({

    template: JST['dashboard/rightbar'],
    el: "#sidebar-right-bb",

    render: function () {
        console.log("RightBar");
        $(this.el).html(this.template());
    }
});