Realync.Views.SearchUser = Backbone.View.extend({

    template: JST['dashboard/search_user'],
    tagName: 'div class="user-item"',

    events: {
        "click #message_client" : "send_message_client"
    },

    send_message_client: function(){

        localStorage.send_users_arr =  JSON.stringify([this.model.get("name")]);
        window.location = "#!/messages";
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        console.log("create search user");
        return this;
    }

});