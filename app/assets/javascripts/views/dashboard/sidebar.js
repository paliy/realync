/**
 * Created by entity on 4/1/14.
 */
Realync.Views.Sidebar = Backbone.View.extend({

    template: JST['dashboard/sidebar'],
    el: "#sidebar-bb",

    render: function () {
        var user =  new Realync.Models.User(JSON.parse(localStorage.user));
        var unread_message = user.unread_message_count();
        console.log("Sidebar");
        if(localStorage.profile_info) {
        	var realtor = JSON.parse(localStorage.profile_info).user.realtor;
        	$(this.el).html(this.template({realtor: realtor}));
        }

        
    }
});