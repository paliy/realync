Realync.Views.File_load = Backbone.View.extend({

    template: JST['dashboard/load_file'],
    el:"#page-content-bb",

    check: function () {
        console.log('tada');
    },

    render: function () {



        var id = '5364d402a584ca1f17ac8edb';

        $(this.el).html(this.template());

         var  uploadButton = $('<button/>')
           .addClass('btn btn-primary')
           .prop('disabled', true)
           .text('Processing...')
           .on('click', function () {
               var $this = $(this),
               data = $this.data();
               $this
               .off('click')
               .text('Abort')
               .on('click', function () {
                 $this.remove();
                 data.abort();
               });
               data.submit().always(function () {
                 $this.remove();
               });
           });
        $('#fileupload').fileupload({

            dataType: 'multipart/form-data',
            async: true,
            cache: true,
            xhrFields: {
                withCredentials: true
            },
            processData: false,
            contentType: false,
            success: function(response) {
                // .. do something
                console.log('--success upload-------------------');
                console.log(response);
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log('--error upload------------------------');
                console.log(jqXHR);

            },


            autoUpload: false,
            maxFileSize: 5000000,
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 100,
            previewMaxHeight: 100,
            previewOrientation: true
        }).on('fileuploadadd', function (e, data) {

                data.context = $('<div/>').appendTo('#files');
                $.each(data.files, function (index, file) {
                    var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                    if (!index) {
                        node
                            .append('<br>')
                            .append(uploadButton.clone(true).data(data));
                    }
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {

                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            }).on('fileuploadprogressall', function (e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }).on('fileuploaddone', function (e, data) {

                $.each(data.result.files, function (index, file) {
                    if (file.url) {
                        var link = $('<a>')
                            .attr('target', '_blank')
                            .prop('href', file.url);
                        $(data.context.children()[index])
                            .wrap(link);
                    } else if (file.error) {
                        var error = $('<span class="text-danger"/>').text(file.error);
                        $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                    }
                });
            }).on('fileuploadfail', function (e, data) {

                $.each(data.files, function (index, file) {
                    var error = $('<span class="text-danger"/>').text('File upload failed.');
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                });
            }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');


        $('#multifileupload').fileupload({
            url: "http://stage.realync.com/property/addPhoto/"+ id,
            dataType: 'multipart/form-data',
            async: true,
            cache: true,
            xhrFields: {
                withCredentials: true
            },
            processData: false,
            contentType: false,
            success: function(response) {
                // .. do something
                console.log('--success upload-------------------');
                console.log(response);
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log('--error upload------------------------');
                console.log(jqXHR);

            },


            autoUpload: false,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 5000000, // 5 MB

            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 100,
            previewMaxHeight: 100,
            previewOrientation: true,
            progressall: function (e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                $('#status_progress .progress_spiner').css(
                    'width',
                    progress + '%'
                );
            }
            //previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            $(".submit#upload").data(data)
            $(".submit#upload").on("click",function(){


                data.submit();
            });

                data.context = $('<div/>').appendTo('#multifiles');
                $.each(data.files, function (index, file) {
                    var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                    if (!index) {
                        node
                            .append('<br>')
                            .append(uploadButton.clone(true).data(data));
                    }
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {

                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.preview) {
                    node
                        .prepend('<br>')
                        .prepend(file.preview);
                }
                if (file.error) {
                    node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            }).on('fileuploadprogressall', function (e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);

            }).on('fileuploaddone', function (e, data) {
                $("submit#upload").on("click",function(){
                    if(sendData){
                        data.formData = $("#file_upload_multiply").serializeArray();
                        sendData = false;
                    }

                    data.submit();
                });

            }).on('fileuploadfail', function (e, data) {

            }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
        // amazon--------------------------



        $(function() {
            $('#file_upload_amazon').fileupload({
                forceIframeTransport: true,
                autoUpload: true,
                add: function (event, data) {
                    $.ajax({
                        url: "/documents/create",
                        type: 'POST',
                        dataType: 'json',
                        data: {doc: {title: data.files[0].name}},
                        async: false,
                        success: function(retdata) {
                            $('#file_upload_amazon').find('input[name=key]').val(retdata.key);
                            $('#file_upload_amazon').find('input[name=policy]').val(retdata.policy);
                            $('#file_upload_amazon').find('input[name=signature]').val(retdata.signature);
                        }

                    });

                    data.submit();
                },
                send: function(e, data) {
                    $('#loading').show();
                },
                fail: function(e, data) {
                    console.log('fail');
                    console.log(data);
                },
                done: function (event, data) {
                    $('#your_documents').load("/documents?for_item=1234");
                    $('#loading').hide();
                }
            });
        });

    }
});
