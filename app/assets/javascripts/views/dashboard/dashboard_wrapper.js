

Realync.Views.DashboardWrapper = Backbone.View.extend({

    template: JST['dashboard/dashboard_wrapper'],
    el: "#page-content-bb",


    events: {
        "click button": "check"
    },
    initialize: function () {
      if(localStorage.user && localStorage.profile_info){
        this.isRealtor = JSON.parse(localStorage.profile_info).user.realtor;
      }
    },

    check: function () {
        console.log('tada');
    },


    render: function () {
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();

    
        if(this.isRealtor){
            var prop = new Realync.Models.Property;
            var prop2 = new Realync.Models.Message;
            prop.index('dashboard');
            prop2.index('dashboard');
            (new Realync.Views.agentRightbar).render();
            (new Realync.Views.agentDashboard).render();
        } else {
            var prop2 = new Realync.Models.Message;
            prop2.index('dashboard');
            (new Realync.Views.Rightbar).render();
            (new Realync.Views.Dashboard).render();
        }



    }
});

var dashboardWrapperView = new Realync.Views.DashboardWrapper;
