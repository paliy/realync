Realync.Views.agentRightbar = Backbone.View.extend({

    template: JST['dashboard/agent_rightbar'],
    el: "#sidebar-right-bb",


    addRecentPropertyList: function(){
    	console.log('Recent Property List')
        var ul = $('.view-recent-properties');
        ul.html("<p>You currently don`t have any " +
                "properties on ReaLync.</p>" +
                "<a href='#!/newProperty' class='button button-transparent'>Add New Property</a>"
                );

        var last_three = Realync.properties_collection.last_three();
        if(last_three.length) {
            ul.html('');
            set_el_spiner('view-recent-properties');
            for(i in last_three){
            	this.addRecentPropertyItem(last_three[i]);
            }
            reset_el_spiner('view-recent-properties');
        }
        
    },

    addRecentPropertyItem: function(property){
    	console.log('Recent Property Item')
        var ul = $('.view-recent-properties');
        var view = new Realync.Views.Recent_properties({model: property});
        ul.append(view.render().el);
    },

    addRecentMessageList: function(){
        console.log('Recent Message List')
        var ul = $('.message_preview_list');
        ul.html("<p>Sorry. You don't have any messages.</p>" +
                "<a href='#!/messages' class='button button-transparent'>Compose Message</a>"
        );

        var last_three = Realync.messages_collection.last_three();
        if(last_three.length) {
            ul.html('');
            set_el_spiner('message_preview_list');

            for(i in last_three){
                this.addRecentMessageItem(last_three[i]);
            }
            reset_el_spiner('message_preview_list');
        }


    },

    addRecentMessageItem: function(message){
        console.log('Recent Message Item')
        var ul = $('.message_preview_list');
        var view = new Realync.Views.Recent_messages({model: message});
        ul.append(view.render().el);
    },

    render: function () {
        console.log("Agent RightBar");
        $(this.el).html(this.template());
        // this.addRecentPropertyList();
    }
});

