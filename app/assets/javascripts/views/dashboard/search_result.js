Realync.Views.SearchResult = Backbone.View.extend({

    template: JST['dashboard/search_result'],
    el: "#page-content-bb",
    events: {
      'click .search-all' : 'showAll',
      'click .search-users' : 'showUsers',
      'click .search-properties' : 'showProperty'
    },

    showAll: function(){
        $(".all-users-search-list, .all-property-search-list").show();
        $("a.search-all").css('color', '#1ca5c9');
        $("a.search-users").css('color', '#a4a4a4');
        $("a.search-properties").css('color', '#a4a4a4');
    },

    showUsers: function(){
        $(".all-users-search-list").show();
        $(".all-property-search-list").hide();
        $("a.search-all").css('color', '#a4a4a4');
        $("a.search-users").css('color', '#1ca5c9');
        $("a.search-properties").css('color', '#a4a4a4');
    },

    showProperty: function(){
        $(".all-users-search-list").hide();
        $(".all-property-search-list").show();
        $("a.search-all").css('color', '#a4a4a4');
        $("a.search-users").css('color', '#a4a4a4');
        $("a.search-properties").css('color', '#1ca5c9');
    },

    addSearchResult: function(){
        var users = Realync.search_collection.getUsers();
        var property = Realync.search_collection.getProperties();

        $(".all-users-search-list").html("");
        $(".all-property-search-list").html("");

        for(i in users){
            console.log(users[i]);
            var view = new Realync.Views.SearchUser({model: users[i]});

            $(".all-users-search-list").append(view.render().el);
        }

        for(i in property){
            console.log(property[i]);
            var view = new Realync.Views.SearchProperty({model: property[i]});

            $(".all-property-search-list").append(view.render().el);
        }
    },
    view_client: function(){
        var mess_id = $(this).find('.some-item2').attr('data-item');
        var realtor = $(this).find('.some-item2').attr('data-realtor');
        var user = new Realync.Models.User();
        if (realtor == undefined) {
            user.getClientProfile(mess_id)
        } else if (!(JSON.parse(localStorage.profile_info).user.realtor) && (!$.isEmptyObject(JSON.parse(localStorage.profile_info).myAgent))){
                if (JSON.parse(localStorage.profile_info).myAgent.id == mess_id) {
                    user.getAgentProfile(JSON.parse(localStorage.profile_info).myAgent.id);
                } else {
                    user.getAgentProfileSearch(mess_id)
                }
            } else {
            user.getAgentProfileSearch(mess_id)
        }
    },
    view_profile: function(id, realtor){
        var user = new Realync.Models.User();
        if (!realtor) {
            user.getClientProfile(id)
        } else if (!(JSON.parse(localStorage.profile_info).user.realtor) && (!$.isEmptyObject(JSON.parse(localStorage.profile_info).myAgent))){
                if (JSON.parse(localStorage.profile_info).myAgent.id == id) {
                    user.getAgentProfile(JSON.parse(localStorage.profile_info).myAgent.id);
                } else {
                    user.getAgentProfileSearch(id)
                }
            } else {
            user.getAgentProfileSearch(id)
        }
    },
    render: function(){
        $(document).on('click', '.some-item', this.view_client);
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $("a.search-all").css('color', '#1ca5c9');
        $("a.search-users").css('color', '#a4a4a4');
        $("a.search-properties").css('color', '#a4a4a4');
        console.log("Render search");
        this.addSearchResult();
    }

})