Realync.Views.TopProfileTemplate = Backbone.View.extend({

    template: JST['dashboard/top_profile_template'],
    tagName: 'div',



    render: function () {
        if(localStorage.profile_image){
            this.model.set('mobilePicUrl', localStorage.profile_image);
        }

        this.$el.html(this.template(this.model.toJSON()));
        return this;    }
});