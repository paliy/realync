Realync.Views.Invitation_message = Backbone.View.extend({

    template: JST['messages/invitation'],
    el: "#messages_form",
    events: {
      'click .accept_tour': "accept_tour",
      'click .decline_tour': "decline_tour",
      'click #send_decline_message': "send_decline_message"
    },

    send_decline_message: function(){
        console.log(this.model);
        var view = new Realync.Views.Message_Index;
        view.compose_message("decline");
    },

    decline_tour: function(){
        $("#declined-form").show();
        $(".accept_decline").html("You have declined this tour request. <a href='#'>Undo</a>")
        $("#"+ this.model.get("id") +"").parent().parent().fadeOut();

        var data = {};
        var sender = this.model.get("sender");
        data['id'] = sender.id;
        data['picURL'] =  sender.picURL.replace(' ','&&&&');
        data['name'] = sender.name.replace(/ /,'___');

        user_val = JSON.stringify(data);
        $(".select-result").attr("data-select", user_val)

        this.model.decline_tour(this.model.get('id'));
    },

    accept_tour: function(){
        $("#"+ this.model.get("id") +"").parent().parent().fadeOut();
        var view = new Realync.Views.Compose_message;
        view.render();
        this.model.accept_tour(this.model.get('id'));
    },


    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));

    }
});