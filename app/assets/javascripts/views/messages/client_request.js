Realync.Views.Client_request = Backbone.View.extend({

    template: JST['messages/client_request'],
    el: "#messages_form",

    events:{
        'click .delete_request': "delete_message"
    },

    delete_message:function(){
        this.model.delete_message(this.model.get('id'))
    },

    render: function () {
        console.log("render client request!!!!!!!!!!!!!!!!!")
        $(this.el).html(this.template(this.model.toJSON()));
    }
})