Realync.Views.Message_Index = Backbone.View.extend({
    initialize: function() {
       message_index = this
    },

    template: JST['messages/index'],
    el: "#page-content-bb",
    events: {
      'focus input.find': 'show_info',
      'keyup input.find': 'show_user_list'
    },


    new_message_form: function(){
       var view = new Realync.Views.Compose_message;
       view.render();
      console.log('new message');
    },



    show_user_list: function(){
        var data = $('input.find').val();
        console.log(data);
        this.model.find_users(data);
    },

    show_info:function(){
        console.log('show info');
        $('.search_list').show();
    },

    hide_info:function(){
        console.log('hide info');
        $('.search_list').hide();
    },
    render_template: function(){
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        if($('#logo').length ==0){
            (new Realync.Views.Sidebar).render();
        }
        if($('#search').length ==0){
            (new Realync.Views.Topbar).render();

        }


    },

    render: function (id) {
        this.render_template()

        $(this.$el).html(this.template());
        if(id){
            var message =  _.find(Realync.messages_collection.models, function(item){
                return (item.get('id')) == id;
            });

            if(message.get('type') == 'message'){
                var view = new Realync.Views.Chat_message({model: message});
                view.render();
            };
        }else{
            var view = new Realync.Views.Compose_message;
            view.render();
        }
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.messages').children().addClass('active_link_dash');

        $( '.compose').click(message_index.new_message_form)
    }
});