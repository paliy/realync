Realync.Views.Chat_message = Backbone.View.extend({

    template: JST['messages/chat'],
    el: "#messages_form",

    events:{
        'click .reply_message': "reply_message",
        'click .delete_message': "delete_message"
    },

    delete_message:function(){
      this.model.delete_message(this.model.get('id'))
    },

    reply_message:function(){
        var id = JSON.parse(localStorage.profile_info).user.id
        var sender = realtor = _.find(this.model.get('users'), function(user){ return user['id'] == id });

        var pos = '';
        if(this.getPosition(this.model.get('users'), sender) % 2 == 0){ pos = 'left'}else{pos = 'right'};

      var newMessage = $('.message').val();
      if(newMessage){
          this.model.reply_message({newMessage: newMessage}, this.model.get('id'), sender, pos);
          $('.message').val('');
      }
    },

    createChat: function(){
        console.log(this.model.get('messages'));
        var chat = this.model.get('messages') ;
        $("#chat-item-container").html('');
        for(i in chat){

            var model = new Realync.Models.Chat_item(chat[i])
            var sender = realtor = _.find(this.model.get('users'), function(user){ return user['id'] == chat[i].sender });
            model.set({sender: sender});

            var pos = '';
            if(this.getPosition(this.model.get('users'), sender) % 2 == 0){ pos = 'left'}else{pos = 'right'};
            model.set({position: pos});
            if (pos == "left") {
                var view = new Realync.Views.Chat_item({model: model});
                $("#chat-item-container").append(view.render().el);
            } else {
                console.log('raight')
                console.log(model)
                var view = new Realync.Views.Chat_item_right({model: model});
                $("#chat-item-container").append(view.render().el);
            }

        };

    },

    getPosition: function(arrayName,arrayItem) {
       for(var i=0;i<arrayName.length;i++){
        if(arrayName[i]==arrayItem)
            return i;
        }
      },

    render: function () {
        $(this.el).html(this.template());

        var users = this.model.get("users");
        console.log(this.model.get("users"))
        for (i in users){
            if(users[i].id != current_user.id){
                var all_users_in_chat = users[i].name
                $("#chat_name").append("<div class='chat_members'>" + all_users_in_chat + "</div>")
            }
        }
        this.createChat();
        var height = window.innerHeight-100;
        if ($('#messages_form').innerHeight() > height) {
            $('#chat-item-container').css({"overflow-y":"scroll", "height":height-228});
        }
    }
});