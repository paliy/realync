Realync.Views.Chat_item = Backbone.View.extend({

    template: JST['messages/chat_item'],
    tagName: 'div class="chat-item"',

    check: function () {
        console.log('tada');
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});