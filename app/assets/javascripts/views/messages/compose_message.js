Realync.Views.Compose_message = Backbone.View.extend({
    initialize: function() {
        message_compose = this
    },

    template: JST['messages/compose_message'],
    el: "#messages_form",


    checkForExistUser: function(){
        var users = $('.select-result');
        var user_ids = [];
        var current_user = JSON.parse(localStorage.user)
        user_ids.push(current_user.id);

        users.each(function(){
            var user_id = JSON.parse( $(this).attr("data-select"));
            user_ids.push(user_id.id);

        })
        var mess_arr = Realync.messages_collection.models;

        var chat =  _.filter(mess_arr, function(message){
            var checker = true;

            if(message.get('users').length == user_ids.length){
                var message_ids = [];

                for (i in message.get('users')){
                    message_ids.push(message.get('users')[i].id)
                }

                console.log("FOR!!!");
                for (i in user_ids){

                    console.log(user_ids[i]);
                    console.log(message_ids);
                    if($.inArray(user_ids[i], message_ids) == -1){
                        checker = false;
                    }
                }
                console.log(checker);
            }else{
                checker = false;
            }
            return checker
        });

        return chat
    },

    compose_message: function(ecline){

        if($('.message').val() && $('input.user_search').val()){
            var data = {};
            var current_user = JSON.parse(localStorage.user)
            var message = $('.message').val();
            var users = $('.select-result')
            data.users = '[';
            data.users += '{'+
                '"id":"'+ current_user.id + '",         \
               "name":"'+current_user.name+'",                   \
               "picURL":"'+current_user.mobilePicUrl+'"'
                +'},'
            i = 1;
            users.each(function(){

                var v = JSON.parse( $(this).attr("data-select"));
                v['name'] = v['name'].replace(/___/,' ');
                v['picURL'] = v['picURL'].replace('&&&&',' ');
//             v['type'] = 'message';
//             v['message'] = message;
                data.users += JSON.stringify(v);
                if(i < $('.select-result').length){
                    data.users += ',';
                }
                i +=1;

            })
            data.users += ']';
            data.message = message;
            data.type = 'message';

            if(message_compose.checkForExistUser().length == 0){
                console.log("Send");
                var user = new Realync.Models.User
                user.compose_message(data);
            }else{
                console.log("Open");
                var model = new Realync.Models.Message;
                var mess_bar_id = message_compose.checkForExistUser()[0].get('id');
                model.reply_message({newMessage: message}, mess_bar_id, 'answer');
            }
        }
    },

    render: function () {
        $(this.el).html(this.template());
        document.getElementById('content').style.height = (window.innerHeight-100) +'px' ;
        $('input.user_search').select2({
            multiple: true,
            placeholder: "User`s name",
            minimumInputLength: 1,
            formatInputTooShort: castomSign,
            ajax: {
                url: localStorage.url + '/user/searchUserList',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//                contentType: "application/json; charset=UTF-8",  // this is optional
                crossDomain: true,
                dataType: 'json',


                data: function (term, page) { // page is the one-based page number tracked by Select2

                    return {

                        searchTerm: term //search term

                    };
                },
                results: function (data, page) {
                    var users = _.filter(data.users, function (u){return u.id != current_user.id})
                    return {results: users};
                }
            },

            initSelection: function(element, callback) {

                $.ajax( localStorage.url + '/user/searchUserList', {
                    data: {
                        searchTerm: localStorage.send_users_name
                    },
                    type: 'POST',
                    xhrFields: {
                        withCredentials: true
                    },
                    dataType: 'json'
                }).done(function(data) {
                    var arr_users = JSON.parse(localStorage.array_to_input);
                    arr_users.push(data.users[0]);
                    localStorage.array_to_input = JSON.stringify(arr_users);
                    callback(arr_users);
                });

            },

            formatResult: movieFormatResult, // omitted for brevity, see the source of this page
            formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            formatNoMatches: formatNotFound,
            escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
        });


        function castomSign() {
            return '<div>Please enter the character for searching...</div>'
        };
        function formatNotFound(term) {

            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            if(validateEmail(term)){
                return "<a href='#' onclick='alert('" + term + "');'"
                    + "id='newClient'>Add New Client</a>";
            }else{
                return  '<div>We`re sorry. There are no results for <b>' + $('.select2-input').val()+ '</b></div>'
            }


        };

        function movieFormatResult(user) {
            var markup = "<div class='movie-result'>";
            if(user.mobilePicUrl == "/images/noPhoto.jpeg"){
                user.mobilePicUrl = localStorage.url + "/images/noPhoto.jpeg";
            }
            markup += "<div class='user-avata'><img class='round' src='" + user.mobilePicUrl + "'/></div>";
            markup += "<div class='user-name'><div>" + user.name + "</div></div>";

            markup += "</div>";
            return markup;
        };

        function movieFormatSelection(user) {

            var li = '<div>';
            li += "<div class='movie-result'>";
            li += "<img class='round' src='" + user.mobilePicUrl + "'/>";
            li += "<div class='user-name' >" + user.name + "</div>";
            li += "<div class='user-id'><div style='visibility:hidden' id='" + user.id + "'></div></div>";
            li += "</div>";
            li += '</div>';
            var input = $('input.user_search');

            var data = {};
             data['id'] = user.id;
            data['picURL'] =  user.mobilePicUrl.replace(/ /g,'&&&&');
             data['name'] = user.name.replace(/ /g,'___');

             user_val = JSON.stringify(data);
            return "<div class='select-result' data-select=" + user_val+ " type='hidden'</div>" + user.name ;
//
        };
        var users_arr = localStorage.send_users_arr;

        if(users_arr && JSON.parse(users_arr).length != 0){
            delete localStorage.array_to_input;
            localStorage.array_to_input = JSON.stringify([]);
            localStorage.setInitUsers = "true";

            for (i in JSON.parse(users_arr)){
               localStorage.send_users_name = JSON.parse(users_arr)[i];
               $("input.user_search").select2('val', 192);
            }

           delete localStorage.send_users_arr;
           delete localStorage.send_users_name;
        }
        $( '.send_message').click(message_compose.compose_message)
    }
});