Realync.Views.Message_list = Backbone.View.extend({

    template: JST['messages/message_list'],
    el: "#message_list",

    view_message: function(){

        $('#messages .message-item').removeClass('active');
        $(this).addClass('active');

        var mess_id = $(this).find('.item').attr('id');
        var message =  _.find(Realync.messages_collection.models, function(item){
            return (item.get('id')) == mess_id;
        });

        if(message.get('type') == 'message'){
            if(message.get('message')){
                var view = new Realync.Views.Client_request({model: message}) ;
                view.render()
            }else{
                var view = new Realync.Views.Chat_message({model: message}) ;
                view.render()
            }

        }else if(message.get('type') == 'invitation'){
            var user_agent = JSON.parse(localStorage.profile_info).user.realtor

            var view = new Realync.Views.Invitation_message({model: message});
            view.render()

        }else if(message.get('type') == 'request'){
            var view = new Realync.Views.Client_request({model: message}) ;
            view.render()
        }

        message.showMessage(mess_id);
    },

    addMessages_list: function(){
        var ul = $('#messages');
        ul.html('');
        Realync.messages_collection.each(this.addMessageItem, this);
    },

    addMessageItem: function(message){
        var ul = $('#messages');
        var view = new Realync.Views.Message_Index_List({model: message});
        ul.append(view.render().el);
    },

    render: function () {
        $(document).on('click', '.message-item', this.view_message);
        $(this.el).html(this.template());
        this.addMessages_list();
        var height = window.innerHeight-100;
        if ($('#messages_list').innerHeight() > height) {
            $('#message_list').css({"overflow-y":"scroll", "height":height-128});
        }
    }
});

