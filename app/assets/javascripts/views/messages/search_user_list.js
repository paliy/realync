Realync.Views.Search_user_list = Backbone.View.extend({

    template: JST['messages/user_list'],
    el: ".search_list",

    addUsers_list: function(){
        var ul = $('#user_list');
        ul.html('');
        console.log(this.model)
        Realync.search_user_list_collection.each(this.addMessageItem, this);
        $('#user_list li').click(function(){

            var id = $(this).find('.user_id').attr('val')
            var src = $(this).find('img').attr('src');
            var name = $.trim($(this).find('.name').html())

            $('.find').val(name);
            $('.receiver_id').val(id);
            $('.receiver_image').val(src);
        })
    },
    addMessageItem: function(user){
        var ul = $('#user_list');
        var view = new Realync.Views.User_search_list({model: user});
        ul.append(view.render().el);
    },

    render: function () {
        console.log("user_list");
        $(this.el).html(this.template());
        this.addUsers_list()
    }
});