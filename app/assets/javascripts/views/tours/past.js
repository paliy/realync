Realync.Views.Past = Backbone.View.extend({

    template: JST['tours/past'],
    el: "#page-content-bb",

    addPastTour: function(tour){
        console.log("one Past Tour");

        var view = new Realync.Views.PastTourItemList({model: tour});

        $('#past-tours').append(view.render().el);
    },

    addAllPastTours: function(){
        console.log("all Tours");

        $("#past-tours").html("");
        Realync.past_tours_collection.each(this.addPastTour, this);
    },

    render_styles: function() {
        $('a.upcoming-tours-link').css('color', '#a4a4a4');
        $('a.past-tours-link').css('color', '#008cba');
    },


    render: function(){
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".link2 a").css({"color":"#1ca5c9"});
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.tours').children().addClass('active_link_dash');

        this.render_styles();
        this.addAllPastTours();
    }

});