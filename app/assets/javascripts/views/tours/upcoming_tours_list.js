Realync.Views.upcoming_tours_list = Backbone.View.extend({

    template: JST['tours/upcoming_tours_list'],
    el: "#upcoming_list",

    addUpcomingTour: function(tour){
        console.log("one prop");

        var view = new Realync.Views.Dashboard.Tours_item({model: tour});

        $('.dashboard-tours').append(view.render().el);

        $("#" + tour.get("id")).html("");
        var attendees = tour.get("accessList");

        for (a in attendees){
            if(a < 4){
                var view = new Realync.Views.Attendies({model: attendees[a]});
                $("#" + tour.get("id")).append(view.render().el);
            }else{
                var view = new Realync.Views.Many_attendies({ model: { length: attendees.length - 4 } });
                $("#" + tour.get("id")).append(view.render().el);
                return ;
            }
        }
    },

    addAllUpcomingTours: function(){
        console.log("all prop");
        $(".dashboard-tours").html("");
        Realync.upcoming_collection.each(this.addUpcomingTour, this);
    },


    render: function () {
        $(this.el).html(this.template());
        this.addAllUpcomingTours();
    }
});