Realync.Views.Attendies = Backbone.View.extend({

    template: JST['tours/attendies_item'],
    tagName: 'div class="attend_item"',

    render: function () {
        this.$el.html(this.template(this.model));
        return this;
    }

});