Realync.Views.PastTourItemList = Backbone.View.extend({

    template: JST['tours/past_tour_item'],
    events: {
        "click .send_message_to_attend" : "send_message",
        "click .start_view_tour" : "start_view_tour",
        "click .delete_tour" : "delete_tour"
    },

    delete_tour: function(){
        console.log('delete');
        console.log('delete');
        console.log(this.model);
        var id = this.model.get('id');
        $("." + id).fadeOut();
        this.model.deleteVideo(id);
        return false
    },

    start_view_tour: function(){
        window.location = "#!/tour/show/" + this.model.get("id");
//        this.model.createTourOpentok(this.model.get("videoSession"), "agent");

    },


    send_message: function(){
        var arr_access_list = this.model.get("accessList");
        var arr = [];
        for (i in arr_access_list){
            if(arr_access_list[i].name){
                arr.push(arr_access_list[i].name);
            }
        }
        localStorage.send_users_arr =  JSON.stringify(arr);
        window.location = "#!/messages";
        return false;
    },

    render: function () {
        var rating_string = [0, 0, 0, 0, 0];
        var full_rating = this.model.get('fullRating'); 
        if(full_rating > 0) {   
            rating_string = [];     
            $('.'+ this.model.get('id') +' .score').html('');
            for(var i = 0; i < full_rating; i++) {
                rating_string.unshift(1);
            }
            for(var i = 0; i < (5 - full_rating); i++) {
                rating_string.unshift(0);
            }
        } 
        this.model.set('string_rating', rating_string);
        var agent = JSON.parse(localStorage.profile_info).user.id == this.model.get('realtor');
        this.model.set('agent', agent);
        console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa');
        console.log(this.model);
        console.log(this.model.get('fullRating'));
        this.$el.html(this.template(this.model.toJSON()));
        console.log("create tour item list");

        return this;
    }

});