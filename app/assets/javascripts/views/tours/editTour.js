Realync.Views.EditTour = Backbone.View.extend({

    template: JST['tours/edit_tour'],
    el: "#page-content-bb",
    events: {

    },

    addPropertyItem: function(property) {
        var availableTags = [];
        availableTags.push(property.id);
        var view = new Realync.Views.Select_property({model: property});
        $('.helpProperty').append(view.render().el);
    },

    addProfileInfo: function(){
        var user = {}
        if( localStorage.user){
            user = JSON.parse(localStorage.profile_info).user;
        }
//        var user = new Realync.Models.User(user );

        var li = '<div>';
        li += "<div class='movie-result'>";
        li += "<div class='attendee-wrap'>";
        li += "<img class='round' src='" + user.mobilePicUrl + "'/>";
        li += "<div class='user-name' >" + user.name + "</div>";
        li += "<div class='user-id'><div style='visibility:hidden' id='" + user.id + "'></div></div>";
        li += "</div>";
        li += '</div>';

//        var view = new Realync.Views.TopProfileTemplate({model: user});
        $('#current-user').html(li);
    },

    render_laout: function(){
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
    },

    add_calendar: function(){
        var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
        var start_date = this.model.get("startDate");
        var word_data = this.model.get("word_data");
        var day_name = word_data.substring(0, 3);
        var day = new Date(start_date).getDate();
        var month = new Date(start_date).getMonth();
        var year = new Date(start_date).getFullYear();

        $('input[name="dateTime"]').datetimepicker({
            datepicker: false,
            timepicker: false,
            value: day_name+' '+monthNames[month] +' '+ day + ' '+ year +'                                      ' +this.model.attributes.hours
        });
        $('#calendar_div').datetimepicker({
            format: 'D M d Y                                      H:i',
            onChangeDateTime: function(current_time, $input) {
                $('input[name="dateTime"]').val(current_time.dateFormat('D M d Y                                    H:i'));
                if ($('input[name="dateTime"]').val() != "") {
                    $(".dateTime_place").css({"display":"block"});
                }
            },
            step:5
        });

    },

   add_property_selector: function(){
       var arr = Realync.properties_collection.sort_for_select();
       var property_id = this.model.get('property').id;

       function format(state) {
           console.log(state);
           if (!state.id) return state.text; // optgroup
           return "<img class='flag' src='" + state.src + "'/>"  + "<div class='property_in_form'>"+ state.text +"</div>" ;
       }
       var property_for_schedule = _.find(arr, function(element){return element.id == property_id });
       $('input[name="propertyID"]').select2({
           multiple: true,
           maximumSelectionSize: 1,
           formatResult: format,
           formatSelection: format,
           data: arr,

           initSelection: function (element, callback) {
               console.log(element);
               var data = property_for_schedule;
               callback(data);
           }
       });
       $('input[name="propertyID"]').select2('val', 193);
    },

  add_user_selector: function(){
      var current_user = JSON.parse(localStorage.profile_info).user;
      var user_names = [];
      var users = this.model.get('accessList');
      for(i in users){
          if(current_user.name != users[i].name){
              user_names.push(users[i].name);
          }
      };


      $('input[name="attendees"]').select2({
          multiple: true,
          placeholder: "Search for a movie",
          minimumInputLength: 1,
          formatInputTooShort: customSign,
          ajax: {
              url: localStorage.url + '/user/searchUserList',
              type: 'POST',
              contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//                contentType: "application/json; charset=UTF-8",  // this is optional
              crossDomain: true,
              dataType: 'json',


              data: function (term, page) { // page is the one-based page number tracked by Select2
                  console.log(term);
                  return {

                      searchTerm: term //search term

                  };
              },
              results: function (data, page) {
                  var users = _.filter(data.users, function (u){
                      console.log(u.name)
                      console.log(user_names)
                      console.log($.inArray(u.name, user_names))
                      return !u.realtor && ($.inArray(u.name, user_names) == -1)})
                  console.log(data);
//                    var more = (page * 10) < data.total; // whether or not there are more results available

                  // notice we return the value of more so Select2 knows if more results can be loaded
                  return {results: users};
              }
          },
          initSelection: function(element, callback) {

              $.ajax( localStorage.url + '/user/searchUserList', {
                  data: {
                      searchTerm: client_users_name
                  },
                  type: 'POST',
                  xhrFields: {
                      withCredentials: true
                  },
                  dataType: 'json'
              }).done(function(data) {
                  console.log('---------------init SELECTION-------------------------')
//                  var arr_users = JSON.parse(localStorage.array_to_input);
//                  arr_users.push(data.users[0]);
//                  localStorage.array_to_input = JSON.stringify(arr_users);
                  callback(data.users[0]);
              });

          },

          formatResult: movieFormatResult, // omitted for brevity, see the source of this page
          formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
          dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
          formatNoMatches: formatNotFound,
          escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
      });


      function customSign() {
          return '<div class="search_div">Search for a person in Realync by name or email address,' +
              '       or enter an email address to invite someone new.</div>'
      };
      function formatNotFound(term) {
          console.log(term);
          console.log(term);
          function validateEmail(email) {
              var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return re.test(email);
          }
          if(validateEmail(term)){
              return "<div class='search_div'>" + "<b>" + term + "</b>" + '? We don`t know that person. Add their name and select "Invite User". '
                  + 'We`ll send them an invite email. Once they sign up, you`ll be added as their agent and they`ll recieve an invite to access your tour. </br></br></br>'

                  + "<div class='full_name'><b> FULL NAME </b>" +
                  "<input class='full-name' type='text'/>"+
                  "</div>" +
                  "<input type='submit' class='button' value='Invite User'>"
                  + "</div>";
//                "<a href='#' onclick='alert('" + term + "');'"
//                    + "id='newClient'>Add New Client</a>";
          }else {
              return  "<div class='search_div'>" + "We`re sorry. There are no results for " + "<b>"+ term +"</b>" + "</div>"
          }


      }
      function movieFormatResult(user) {
          var current_user = new Realync.Models.User(JSON.parse(localStorage.user));

          var markup = "<div class='movie-result'>";

          if(user.mobilePicUrl == "/images/noPhoto.jpeg"){
              user.mobilePicUrl = localStorage.url + "/images/noPhoto.jpeg";
          }
          markup += "<div class='user-avata'><img class='round' src='" + user.mobilePicUrl + "'/></div>";
          markup += "<div class='user-name'><div>" + user.name + "</div></div>";
          markup += "</div>";
          return markup;

      }
      function movieFormatSelection(user) {
          var li = '<div>';
          li += "<div class='movie-result'>";
          li += "<div class='attendee-wrap'>";
          li += "<img class='round' src='" + user.mobilePicUrl + "'/>";
          li += "<div class='user-name' >" + user.name + "</div>";
          li += "<div class='user-id'><div style='visibility:hidden' id='" + user.id + "'></div></div>";
          li += "</div>";
          li += "<div class='remove-attendee' id='" + user.id + "'><img class='remove-icon' src='" + "assets/remove-attendee.png" + "'/></div>";
          li += '</div>';

          $('div.attendees-container').append(li);

          $('.remove-attendee').click(function(){
              var attendees = $('input[name="attendees"]').val();
              var spl = attendees.split(',');
              var user_id = $(this).attr('id');
              spl.forEach(function(val) {
                  if(val == user_id) {
                      spl.splice(spl.indexOf(val), 1);
                  }
              })
              $('input[name="attendees"]').val(spl.join(','));
              $(this).parent('.movie-result').remove();
          });


//            return user.name;

      };
      var client_users_name = '';
      localStorage.array_to_input = JSON.stringify([]);
      console.log(user_names);
      for (i in user_names){
          console.log('SET USER')
          console.log(user_names[i])
          console.log('SET USER')
          client_users_name = user_names[i];
          $('input[name="attendees"]').select2('val', 192);
      }


    },


    create_form: function(){

        function add_attendees(){
            var arr = '[';
            var i = 1;
            var user_container = $('div.attendees-container div div.attendee-wrap' )
            user_container.each(function(){
                arr += '{                                         \
                  "id": "' + $(this).find(' .user-id div').attr('id') + '",\
                  "name": "' + $.trim($(this).find('.user-name').text()) + '",\
                  "picURL": "' + $(this).find('img').attr('src') +'"\
                }';

                if(i < user_container.length){ arr += ','; }
                i +=1;
            });
            arr += ']';
            console.log(arr);
            return arr;
        };

        var tour = this.model;
        var form = new Backbone.Form({
            template: _.template($('#editTourTemplate').html()),
            model: tour
        }).render();
        $('#tour').html(form.el);


        form.on('submit', function(event) {
            var data = form.getValue();
           var errors = form.commit();
            console.log(errors);
            data.clients = add_attendees();
            var arr = data.dateTime.split("  ");
            var min = new Date($.now()).getTimezoneOffset();
            data.dateTime = arr[0] + " " + arr[arr.length -1] + tour.getHours(min);
            data.isOpenHouse = false;
            data.userID = current_user.id;
            var tour_id = this.model.get("id")

            if(! errors){
               tour.editTour(data, tour_id);
            };
        });
    },

    render: function () {
        console.log('edit  tour');
        console.log(this.model);
        model = this.model;
        $(this.el).html(this.template(this.model.toJSON()));
        this.render_laout();
        this.create_form();
        this.add_calendar();
        this.add_property_selector();
        this.add_user_selector();
        this.addProfileInfo();

    }

});