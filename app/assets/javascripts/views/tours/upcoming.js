Realync.Views.Upcoming = Backbone.View.extend({

    template: JST['tours/upcoming'],
    el: "#page-content-bb",

    events: {
        "click .join-tour" : "join_tour"
    },

    addUpcomingTour: function(tour){
        console.log("one Tour");

        var view = new Realync.Views.TourItemList({model: tour});

        $('#upcoming-tours').append(view.render().el);
        var view = new Realync.Views.TourList;
        view.set_attendies(tour);
    },


    addAllUpcomingTours: function(){
        console.log("all Tours");

        $("#upcoming-tours").html("");
        Realync.client_tours_collection.each(this.addUpcomingTour, this);
    },

    render_topbar: function(){
      if($('#topbar-bb img').length == 0){
       (new Realync.Views.Topbar).render();
      }
    },

    render_styles: function() {
        $('a.upcoming-tours-link').css('color', '#008cba');
        $('a.past-tours-link').css('color', '#a4a4a4');
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.tours').children().addClass('active_link_dash');
    },

    render: function(){
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        this.render_topbar();
        this.render_styles();
        this.addAllUpcomingTours();

        var view = new Realync.Views.TourList;
        view.upcomingToursClient();
    }

});

