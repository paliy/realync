Realync.Views.Many_attendies = Backbone.View.extend({

    template: JST['tours/many_attendies'],
    tagName: 'div class="attend_item"',

    render: function () {
        this.$el.html(this.template(this.model));
        return this;
    }

});