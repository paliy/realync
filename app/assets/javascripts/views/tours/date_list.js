Realync.Views.DateList = Backbone.View.extend({

    template: JST['tours/date_list'],
    tagName: 'div class="datetime-block clearfix"',

    render: function () {
        this.$el.html(this.template(this.model));
        console.log("create date list");
        return this;
    }

});