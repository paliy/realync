Realync.Views.TourList = Backbone.View.extend({
    initialize: function() {
        tour_view = this;
    },

    template: JST['tours/list'],
    el: "#page-content-bb",


    addTour: function(){
        var view = new Realync.Views.TourLists({model: tour});
        $('.all-tour-list').append(view.render().el);

    },

    get_month: function(i){
        //var d = new Date(data);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var n = month[i];
        return n
    },

    get_day: function(i){
        //var d = new Date(data);
        var d = i % 6;
        var week = new Array();
        week[0] = "Sunday";
        week[1] = "Monday";
        week[2] = "Tuesday";
        week[3] = "Wednesday";
        week[4] = "Thursday";
        week[5] = "Friday";
        week[6] = "Saturday";
        var n = week[d];
        return n
    },


    set_attendies: function(tour){
        $("#" + tour.get("id")).html("");
        var attendees = tour.get("accessList");

        for (a in attendees){
            if(a < 4){
              var view = new Realync.Views.Attendies({model: attendees[a]});
              $("#" + tour.get("id")).append(view.render().el);
            }else{
              var view = new Realync.Views.Many_attendies({ model: { length: attendees.length - 4 } });
              $("#" + tour.get("id")).append(view.render().el);
              return ;
            }

        }
    },

    addAllTour: function(){
        var agent_list = Realync.agent_tour_collection.sort_by_days();
        var element =  Realync.past_tours_collection.sort_by_days(agent_list);
        element = _.sortBy(element, function(num){
            var month = (num.month < 10)? '0' + num.month : num.month;
            var day = (num.day < 10)? '0' + num.day : num.day;
            return [month, day].join('|'); });

        $(".all-tour-list").html("");
        console.log('GLOBAL LISTelement');
        console.log(element);
        for(i in element){
//            console.log(element[i]);
//            console.log(element[i]);
            console.log(element[i].tours);
            var view = {};

                view = new Realync.Views.DateList({model: {day_s: this.get_day(element[i].day),
                    month_s: this.get_month(element[i].month),
                    year: element[i].year,
                    day: element[i].day,
                    month: element[i].month}});



            $(".all-tour-list").append(view.render().el);

            for(t in element[i].tours){
                var view = {};
                if(element[i].tours[t].get('past')){
                  console.log('ADDPASTTOUR_________________________________________')
                  view = new Realync.Views.PastTourItemList({model: element[i].tours[t]});
                }else{
                    console.log('ADD NO PAST______________________________________')
                    view = new Realync.Views.TourItemList({model: element[i].tours[t]});
                }
                $("." + element[i].day + '.' + element[i].month + '> .' + element[i].day).append(view.render().el);
                tour_view.set_attendies(element[i].tours[t]);
            }

        }
    },

    upcomingToursClient: function() {
        var upcoming_list = Realync.client_tours_collection.sort_by_days();
        console.log(upcoming_list);

        $('#upcoming-tours').html('');

        for(i in upcoming_list){
            console.log(upcoming_list[i].tours);
            var put_date = {};
                put_date = new Realync.Views.DateList({model: {day_s: this.get_day(upcoming_list[i].day),
                    month_s: this.get_month(upcoming_list[i].month),
                    year: upcoming_list[i].year,
                    day: upcoming_list[i].day,
                    month: upcoming_list[i].month}});
            $("#upcoming-tours").append(put_date.render().el); 
            for(t in upcoming_list[i].tours){
                var view = {};
                view = new Realync.Views.TourItemList({model: upcoming_list[i].tours[t]});
                $("." + upcoming_list[i].day + '.' + upcoming_list[i].month + '> .' + upcoming_list[i].day).append(view.render().el);
                tour_view.set_attendies(upcoming_list[i].tours[t]);
            }   
        }
    },

    monthSelector: function(){
      var month = new Date($.now()).getMonth();
      var year = new Date($.now()).getFullYear();
      $("#date-selector .date .month ").html("");
      $("#date-selector .date .year ").html("");
      $("#date-selector .month").html(this.get_month(month));
      $("#date-selector .year").html(year);

      $("#date-selector .date").attr("date-month", month);
      $("#date-selector .date").attr("date-year", year);
      $("." + month + "[data-year = '" + year +  "']").parent().css('display','block');
    },

    render: function () {
        $(this.el).html(this.template());
        (new Realync.Views.Sidebar).render();
        (new Realync.Views.Topbar).render();
        $(".large-4.columns#rightbar").fadeOut();
        $("#content").removeClass('large-8').addClass('large-12');
        $("a.calendar-link").css('color', '#a4a4a4');
        $("a.list-link").css('color', '#1ca5c9');
        $('.dashboard_links a span').removeClass('active_link_dash');
        $('.dashboard_links a.tours').children().addClass('active_link_dash');

        //this.checkTourList();
        this.addAllTour();
        this.monthSelector();
    }
})

$(document).on("click", "#span-to-left", function(){
    var set_month = $(this).parent().find(".date").attr("date-month");
    var set_year = $(this).parent().find(".date").attr("date-year");
    var prew_month = set_month - 1;

    if(prew_month < 0){
        set_year -= 1;
        prew_month = 11;
        $("#date-selector .date").attr("date-month", prew_month);
        $("#date-selector .month").html(tour_view.get_month(prew_month));
        $("#date-selector .date").attr("date-year", set_year);
        $("#date-selector .year").html(set_year);
    }else{
        $("#date-selector .date").attr("date-month", prew_month);
        $("#date-selector .month").html(tour_view.get_month(prew_month));
    }
    $(".datetime-block").css('display', 'none');
    $("." + prew_month + "[data-year = '" + set_year +  "']").parent().css('display','block');

})

$(document).on("click", "#span-to-right", function(){
    var set_month = $(this).parent().find(".date").attr("date-month");
    var set_year = $(this).parent().find(".date").attr("date-year");
    var next_month = parseInt(set_month) + 1;
    $("#date-selector .date").attr("date-month", next_month);
    $("#date-selector .month").html(tour_view.get_month(next_month));

    if(next_month > 11){
        set_year = parseInt(set_year) + 1;
        next_month = 0;
        $("#date-selector .date").attr("date-month", next_month);
        $("#date-selector .month").html(tour_view.get_month(next_month));
        $("#date-selector .date").attr("date-year", set_year);
        $("#date-selector .year").html(set_year);
    }else{
        $("#date-selector .date").attr("date-month", next_month);
        $("#date-selector .month").html(tour_view.get_month(next_month));
    }
    $(".datetime-block").css('display', 'none');
    $("." + next_month + "[data-year = '" + set_year +  "']").parent().css('display','block');
})