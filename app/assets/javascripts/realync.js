window.Realync = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {

  }
};

function set_spiner(){
  var el = $('#realync_spiner');
    var height = $('#page-wrapper').height() + $('#topbar-bb').height() + 5;
  console.log('set spiner');
  el.css('height', height + 'px');
  el.css('width', '100%');
}


function reset_spiner(){
  var el = $('#realync_spiner');
  console.log('set spiner');
  el.css('height', '0');
  el.css('width', '0');
}

function set_progres(param, image){
  var el = $('#status_progress');
  var content = el.find('#progresscontent')
  var img = '';
  if(image == 'image'){
    img =  "<img src='http://localhost:3000/assets/Spinner.gif'>"
  }else{
    img = "<div class='container-pr'><div class='progress_spiner' style='width:"+image+"%'></div></div>"
    img += "<div class='container-pr-multy'><div class='progress_spiner_multy' style='width:"+image+"%'></div></div>"
    img += "<div class='container-pr-video'><div class='progress_spiner_video' style='width:"+image+"%'></div></div>"
  }
  el.find('#progress-content').html(param)
  el.find('#progress-image').html(img)
    set_spiner()

  console.log('set spiner');
    el.css('display', 'block');
}

function sync_messages(){
    $.ajax({
        url: localStorage.url + '/user/unreadMessageCount',

        type: 'GET',

        xhrFields: {
            withCredentials: true
        },

        success: function(output, status, xhr){
            if(output.unreadMessageCount != 0){
                $("#unread-messages").html("");
                $("#unread-messages").append(output.unreadMessageCount).show();
            }
            if(window.location.hash == "#!/messages"){
                message_model.index();
            }
        },
        error: function(data){
        }

    })

}

setInterval(sync_messages, 5000);

function set_process_text(text, type){
    $('#prop-'+type+'-photo .sign').html(text);
}

function check_downloades(){
    var multy = $('#prop-multy-photo').css('z-index');
    var cover = $('#prop-cover-photo').css('z-index');
    var video = $('#prop-multy-photo').css('z-index');
    if(multy == 1 && cover ==1 && video ==1){
      $('#property-progres-bar').css('display', 'none');
      Backbone.history.loadUrl();
    }
}

function set_process_progres(type, progress){
    $('#property-progres-bar').css('display','block');
    $('#prop-'+type+'-progress').css(
        'width',
        progress + '%'
    );

}

function reset_progres(){
  var el = $('#status_progress ');
  console.log('set spiner');
    el.css('display', 'none');
    reset_spiner();
}

Backbone.history.bind("all", function (route, router) {
    $('span.tooltip').hide();
});

$(document).ready(function(){
    Realync.initialize();
    Backbone.history.start();
    // $('[title]').mouseover(function() {return false;});
    // $('[title]').mouseout(function() {return false;})
});

//localStorage.url = 'https://api.realync.com'


// localStorage.url = 'http://192.168.2.190'

localStorage.url = 'http://stage.realync.com'

function set_el_spiner(el){
    $('.' + el ).append('<div class = "spiner-el"><img src="/assets/Spinner.gif" style="width:75px"></div>');
}

function reset_el_spiner(el){
    console.log('reset')
    $('.' + el).find('.spiner-el').remove();
    delete $('.' + el).find('.spiner-el');
}

// function hideLayout(){
//     $('sidebar#sidebar-bb').hide();
//     $('#topbar-bb').hide();
//     $('#page-content-bb').css('top', '0px');
// }
// function showLayout(){
//     $('sidebar#sidebar-bb').show();
//     $('#topbar-bb').show();
//     $('#page-content-bb').css('top', '60px');
// }
