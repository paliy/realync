class DocumentsController < ApplicationController


  # generate the policy document that amazon is expecting.


  def generate_url

      # To avoid file collision, we prepend string to the filename
       property_id = "#{params[:property_id]}"
      user_id = "#{params[:user_id]}"
      filename = "#{user_id}/#{property_id }/video/#{params[:filename]}"

      bucket = "videos-s"
      public_key =  "AKIAJMXYMSTVHJCNBGMQ"

      privat_key = "ufnCsh9XDqkfOE2H2eluYRFrWqksXWMfvqdImv9f"




      # Content types
      #content_type = "application/CSV"
      content_type = "video/mp4"

      resource_endpoint = "https://s3-us-west-2.amazonaws.com/#{bucket.to_s}/#{filename}"#{property_id}"#"/video/#{filename}"
      options = {
          :http_verb => "PUT",
          :date => 1.hours.from_now.to_i,
          :resource => "/#{bucket.to_s}/#{filename}",
          :content_type => content_type
      }

      url = build_s3_upload_url(resource_endpoint, "#{public_key.to_s}", "#{privat_key.to_s}", options)
      render :json => {:put_url => url, :file_url => resource_endpoint, :content_type => content_type, :filename => filename }

  end

  private
  def build_s3_str_to_sign(options = {})
    options = {
        :http_verb => "PUT",
        :content_md5 => nil,
        :content_type => nil,
        :date => 1.hours.from_now.rfc822,
        :amz_headers => [],
        :resource => ""
    }.merge(options)

    str = options[:http_verb].to_s + "\n" +
        options[:content_md5].to_s + "\n" +
        options[:content_type].to_s + "\n" +
        options[:date].to_s + "\n" +
        (options[:amz_headers].any? ? (options[:amz_headers].join("\n") + "\n") : "") +
        options[:resource]
  end

  def build_s3_rest_signature(secret_access_key, options = {})
    str = build_s3_str_to_sign(options).force_encoding("UTF-8")
    result = Base64.encode64(OpenSSL::HMAC.digest("sha1", secret_access_key, str)).strip
  end

  def build_s3_upload_url(endpoint, aws_access_key, secret_access_key, signature_options = {})
    signature = ERB::Util.url_encode(build_s3_rest_signature(secret_access_key, signature_options))
    expires = signature_options[:date] || 1.hours.from_now.rfc822
    "#{endpoint}?AWSAccessKeyId=#{aws_access_key}&Expires=#{expires}&Signature=#{signature}"
  end



end