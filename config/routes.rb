Realync::Application.routes.draw do
  root 'pages#landing'

  get '/sessions', :to => 'pages#sessions', :as => :sessions
  get '/dashboard', :to => 'pages#dashboard', :as => :dashboard
  get '/landing', :to => 'pages#landing', :as => :landing
  post "/dashboard", :to => 'pages#dashboard', :as => :dashboard_new
  post '/pages/upload_image'
  post '/pages/pay'
  post '/pages/save_image'
  post '/documents/create'
  post '/documents/generate_url'


end
